import asyncio
import logging

from account import AccountClient
import aiohttp.web
import aiohttp_debugtoolbar
import aiohttp_jinja2
import jinja2
import uvloop
from aiohttp_swagger import setup_swagger
from aiopg.sa import create_engine

from shop.db.models import Localization
from shop.jinja2_environment import setup_jinja2_filters, setup_jinja2_context_processors
from shop.middlewares import SERVICE_MIDDLEWARES
from shop.routes import init_routes
from shop.settings import Settings
from shop.utils.enums import Environment


async def database_connection(app: aiohttp.web.Application):
    settings = Settings()

    app['db'] = await create_engine(user=settings.database_user,
                                    password=settings.database_password,
                                    database=settings.database_name,
                                    host=settings.database_host,
                                    port=settings.database_port)

    yield

    app['db'].close()
    await app['db'].wait_closed()


async def localization_updater(app: aiohttp.web.Application):
    while 'db' not in app:
        await asyncio.sleep(1)

    while True:
        new_locales = {}
        try:
            async with app['db'].acquire() as connection:
                cursor = await connection.execute(Localization.__table__.select())
                db_locales = await cursor.fetchall()
            for item in db_locales:
                if item.lang_code not in new_locales:
                    new_locales[item.lang_code] = {}
                new_locales[item.lang_code][item.text_code] = item.text_value
            app['locales'] = new_locales
        except Exception as e:
            logging.error(str(e))
        await asyncio.sleep(app['settings'].locale_update_time)


async def loc_updater_starter(app: aiohttp.web.Application):
    loop = asyncio.get_event_loop()
    task = loop.create_task(localization_updater(app))

    yield

    task.cancel()


def setup_server(settings: Settings) -> aiohttp.web.Application:
    asyncio.set_event_loop_policy(uvloop.EventLoopPolicy())

    app = aiohttp.web.Application(middlewares=SERVICE_MIDDLEWARES)
    app['settings'] = settings
    app['account'] = AccountClient(settings.account_service)

    aiohttp_jinja2.setup(app=app,
                         loader=jinja2.FileSystemLoader(settings.path_to_templates),
                         filters=setup_jinja2_filters(),
                         context_processors=setup_jinja2_context_processors())

    init_routes(app=app)
    if Environment(settings.environment) != Environment.PRODUCTION:
        aiohttp_debugtoolbar.setup(app, intercept_redirects=False)
        setup_swagger(app, swagger_url='/api/reference')
    app.cleanup_ctx.extend([database_connection, loc_updater_starter])
    return app


def create_app():
    settings = Settings()
    app = setup_server(settings)
    return app


if __name__ == '__main__':
    settings = Settings()

    app = setup_server(settings)
    aiohttp.web.run_app(app, host=settings.service_host, port=settings.service_port)
