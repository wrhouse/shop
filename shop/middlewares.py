from typing import Callable

import ujson as json
from aiohttp.web import (
    middleware,
    json_response,
    Request,
    HTTPClientError,
    HTTPServerError,
    HTTPException
)
from aiohttp_jinja2 import render_template

from shop.responses import ServiceException
from shop.settings import Settings

settings = Settings()

@middleware
async def exception_middleware(request: Request,
                               handler: Callable):
    status_code = 500
    error_code = 500
    error_title = 'Internal Server Error'
    error_message = 'Internal Server Error'
    is_ajax = bool(int(request.rel_url.query.get('is_ajax') or '0'))
    if settings.environment == 'dev':
        return await handler(request)
    try:
        response = await handler(request)
    except ServiceException as service_exception:
        status_code = service_exception.status_code
        error_code = service_exception.error_code
        error_title = type(service_exception).__name__
        error_message = service_exception.error_message
    except (HTTPClientError, HTTPServerError) as http_exception:
        status_code = http_exception.status_code
        error_code = http_exception.status
        error_title = type(http_exception).__name__[4:]  # Left strip 'HTTP' prefix
        error_message = http_exception.reason
    except HTTPException as another_http_exception:
        raise another_http_exception from None
    except Exception as e:
        print(e)
    else:
        return response

    if is_ajax:
        data = {
            "success": False,
            "error": {
                "code": error_code or status_code,
                "message": error_message
            },
        }
        return json_response(data=data,
                             status=status_code,
                             dumps=json.dumps)
    else:
        context = {
            "error_code": status_code,
            "error_title": error_title,
            "error_message": f"Error {error_code}: {error_message}",
            "referer": request.headers.get('Referer')
        }
        return render_template(template_name='error.jinja2',
                               request=request,
                               context=context,
                               status=status_code)


SERVICE_MIDDLEWARES = [
    exception_middleware
]
