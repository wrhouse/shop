from typing import Optional

import aiohttp.web

from shop.controllers.templates import routes as template_urls
from shop.utils.urls import include_urls


def init_routes(app: aiohttp.web.Application,
                prefix: Optional[str] = None) -> None:
    include_urls(app, None, template_urls)
