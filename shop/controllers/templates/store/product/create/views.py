import aiohttp.web
from aiohttp_jinja2.helpers import url_for

from shop.db.crud.product import fn_create_product
from shop.decorators import (
    check_store_member_permissions,
    lazy_authorized,
    mixin_user_info,
    identify_store,
    service_template,
    authorized
)
from shop.permissions import MANAGE_LISTINGS
from shop.utils import url_encode


@lazy_authorized(required=True)
@mixin_user_info('nickname', 'image_small')
@identify_store(fields=['id', 'name', 'url'])
@check_store_member_permissions(MANAGE_LISTINGS)
@service_template(template_name='pages/product/create.jinja2', only_template=True)
async def get(request: aiohttp.web.Request):
    store = request['store']
    store_url = request.app.router['store_get'].url_for(**{'store_url': store.url})

    return {
        'title': 'title:product:create',
        'breadcrumbs': [
            (store_url, store.name)
        ]
    }


@authorized(required=True)
@identify_store()
@check_store_member_permissions(MANAGE_LISTINGS)
@service_template(only_json=True)
async def post(request: aiohttp.web.Request):
    request_data = await request.json()

    store = request['store']
    engine = request.app['db']
    settings = request.app['settings']

    await fn_create_product(engine,
                            shop_id=store.id,
                            author_id=request['user_id'],
                            game_id=request_data['game_id'],
                            service_id=request_data['service_id'],
                            platform_id=request_data['platform_id'],
                            game_option_value_ids=request_data['game_option_value_ids'],
                            name=request_data['name'],
                            url=url_encode(request_data['name']),
                            description=request_data['description'],
                            images=request_data['images'],
                            image_size=settings.product_image_size,
                            thumbnail_size=settings.product_thumbnail_size,
                            currency_id=request_data['currency_id'],
                            price_per_unit=request_data['price_per_unit'],
                            stock=request_data['stock'],
                            min_unit_per_order=request_data['min_unit_per_order'],
                            insurance_id=request_data.get('insurance_id'),
                            duration_id=request_data['duration_id'],
                            delivery_option_ids=request_data['delivery_option_ids'],
                            online_hrs=request_data['online_hrs'],
                            offline_hrs=request_data['offline_hrs'])

    return None
