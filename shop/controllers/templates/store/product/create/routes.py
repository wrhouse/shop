from typing import Optional

import aiohttp.web

from shop.controllers.templates.store.product.create.views import get, post


def init_routes(app: aiohttp.web.Application,
                prefix: Optional[str] = None) -> None:
    app.router.add_get(prefix, get, name='store_product_create_get')
    app.router.add_post(prefix, post, name='store_product_create_post')
