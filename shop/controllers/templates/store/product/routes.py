from typing import Optional

import aiohttp.web

from shop.controllers.templates.store.product.create import routes as create_routes
from shop.utils.urls import include_urls


def init_routes(app: aiohttp.web.Application,
                prefix: Optional[str] = None) -> None:
    include_urls(app, prefix + '/create', create_routes)
