from typing import Optional

import aiohttp.web

from shop.controllers.templates.store.views import (
    index
)
from shop.controllers.templates.store.product import routes as product_urls
from shop.utils.urls import include_urls


def init_routes(app: aiohttp.web.Application,
                prefix: Optional[str] = None) -> None:
    include_urls(app, prefix + '/product', product_urls)

    app.router.add_get(prefix, index, name='store_get')
