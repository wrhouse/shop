from typing import Optional

import aiohttp.web

from shop.controllers.templates.ajax import routes as ajax_urls
from shop.controllers.templates.store import routes as store_urls
from shop.controllers.templates.views.about_us import about_us
from shop.controllers.templates.views.for_buyers import for_buyers
from shop.controllers.templates.views.games import games
from shop.controllers.templates.views.index import index
from shop.controllers.templates.views.trades import game_trades, get_trade
from shop.utils.urls import include_urls


def init_routes(app: aiohttp.web.Application,
                prefix: Optional[str] = None) -> None:
    include_urls(app, '/ajax', ajax_urls)
    include_urls(app, '/store/{store_url}', store_urls)

    app.router.add_get("/", index, name='index')
    app.router.add_get("/about_us", about_us, name='about_us')
    app.router.add_get("/games", games, name='games')
    app.router.add_get("/games/{game_url}", game_trades, name='game_trades')
    app.router.add_get("/games/{game_url}/{product_url}-{product_id}", get_trade, name='trade')
    app.router.add_get("/for_buyers", for_buyers, name='for_buyers')

    # views
    # / - title:main - index
    # /for_buyers - title:for_buyers - for_buyers
    # /about_us - title:about_us - about_us

    # products -> views
    # /platforms - title:platforms - platforms_list
    # /platforms/{platform_name} - title:{platform_name} - games_list
    # /platform/{platform_name}/{game_name} - title:{game_name} - trades_list
    # /platform/{platform_name}/{game_name}/{product_name} - title:{product_name} - trade

    # shops -> views
    # /shops - title:shops - shops_list
    # /shops/{shop_name} - title:{shop_name} - shop
    # /shops/{shop_name}/lots - title:lots - lots_list
    # /shops/{shop_name}/lots/{product_name} - title:{product_name} - lot
