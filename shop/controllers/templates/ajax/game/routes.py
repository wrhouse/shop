from typing import Optional

import aiohttp.web

from .views import get_list


def init_routes(app: aiohttp.web.Application,
                prefix: Optional[str] = None) -> None:
    app.router.add_get(prefix + '/list', get_list, name='ajax_game_list_get')
