from aiohttp import web

from shop.db.crud import (
    fn_get_game,
    fn_get_platform
)
from shop.db.models import Game
from shop.decorators import service_template
from shop.utils import json_formatter


get_list_game_json_formatter = json_formatter(['id', 'name', 'url', 'image_url'])
get_list_service_json_formatter = json_formatter(['id', 'name'])


@service_template(only_json=True)
async def get_list(request: web.Request):
    engine = request.app['db']
    query = dict(request.rel_url.query)

    target_platforms = None
    if 'platform_group' in query:
        platforms = await fn_get_platform(engine,
                                          platform_group_name=query['platform_group'])
        target_platforms = [platform.id for platform in platforms]

    target_services = None
    if 'services' in query:
        service_ids = query['services'].replace(' ', '').split(',')
        target_services = [int(service_id) for service_id in service_ids]

    limit = None
    if query.get('limit'):
        limit = int(query['limit'])

    offset = None
    if query.get('offset'):
        offset = int(query['offset'])

    games = await fn_get_game(engine,
                              platforms=target_platforms,
                              services=target_services,
                              limit=limit,
                              offset=offset,
                              order_field=Game.name,
                              extended=True)

    formatted_games = []
    for game in games:
        formatted_games.append(get_list_game_json_formatter(game.game))
        formatted_games[-1]['services'] = []
        for service in game.services:
            if service.parent_id == 0:
                formatted_games[-1]['services'].append(get_list_service_json_formatter(service))

    return formatted_games
