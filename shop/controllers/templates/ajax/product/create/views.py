import aiohttp.web
import aiohttp_jinja2

from shop.db.crud import (
    fn_get_currency,
    fn_get_delivery_option,
    fn_get_game,
    fn_get_game_option,
    fn_get_rel_service_game,
    fn_get_service,
    fn_get_service_duration,
    fn_get_service_insurance,
    fn_get_platform
)
from shop.db.models import Game
from shop.decorators import (
    service_template,
    lazy_authorized
)
from shop.responses import InvalidParameter
from shop.utils import json_formatter

get_platform_json_formatter = json_formatter(['id', 'name'])
get_game_json_formatter = json_formatter(['id', 'name'])
get_service_json_formatter = json_formatter(['id', 'name'])


@lazy_authorized(required=True)
@service_template(only_json=True)
async def get_platform(request: aiohttp.web.Request):
    # TODO: Swagger
    engine = request.app['db']

    platforms = await fn_get_platform(engine)
    json_data = [get_platform_json_formatter(platform) for platform in platforms]
    return json_data


@lazy_authorized(required=True)
@service_template(only_json=True)
async def get_game(request: aiohttp.web.Request):
    # TODO: Swagger
    engine = request.app['db']
    params = dict(request.rel_url.query)

    platform_id = int(params['platform_id'])

    games = await fn_get_game(engine, platforms=[platform_id], order_field=Game.name)
    json_data = [get_game_json_formatter(game) for game in games]
    return json_data


@lazy_authorized(required=True)
@service_template(only_json=True)
async def get_service(request: aiohttp.web.Request):
    #  TODO: Swagger
    engine = request.app['db']
    params = dict(request.rel_url.query)

    game_id = int(params['game_id'])
    parent_id = int(params['parent_id'])

    services = await fn_get_service(engine, parent_id=parent_id, game_id=game_id)
    json_data = [get_service_json_formatter(service) for service in services]
    return json_data


@lazy_authorized(required=True)
async def get_advanced_form(request: aiohttp.web.Request):
    engine = request.app['db']
    settings = request.app['settings']
    params = dict(request.rel_url.query)

    game_id = int(params['game_id'])
    service_id = int(params['service_id'])

    rel_service_game = await fn_get_rel_service_game(engine, game_id=game_id, service_id=service_id)
    if rel_service_game:
        rel_service_game = rel_service_game[0]
    else:
        raise InvalidParameter('game_id or service_id')

    serviceChildren = await fn_get_service(engine, parent_id=service_id)
    if serviceChildren:
        raise InvalidParameter('service_id')

    service = await fn_get_service(engine, service_id=service_id)
    if service:
        service = service[0]
    else:
        raise InvalidParameter('service_id')

    game_options = await fn_get_game_option(engine, game_id=game_id, service_id=service_id, extended=True)
    durations = await fn_get_service_duration(engine, service_id=service_id)
    insurances = await fn_get_service_insurance(engine, service_id=service_id)
    currencies = await fn_get_currency(engine)
    delivery_options = await fn_get_delivery_option(engine, game_id=game_id, service_id=service_id)

    return aiohttp_jinja2.render_template(template_name=f'pages/product/forms/{rel_service_game.advanced_form_name}',
                                          request=request,
                                          context={
                                              'price_unit_placeholder': rel_service_game.price_unit_placeholder,
                                              'game_options': game_options,
                                              'currencies': currencies,
                                              'insurances': insurances,
                                              'durations': durations,
                                              'delivery_guidelines': service.delivery_guidelines,
                                              'delivery_options': delivery_options,
                                              'online_hrs': range(settings.min_online_hrs, settings.max_online_hrs + 1),
                                              'offline_hrs': range(settings.min_offline_hrs, settings.max_offline_hrs + 1)
                                          })
