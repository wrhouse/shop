from typing import Optional

import aiohttp.web

from shop.controllers.templates.ajax.product.create.views import (
    get_platform,
    get_game,
    get_service,
    get_advanced_form
)


def init_routes(app: aiohttp.web.Application,
                prefix: Optional[str] = None) -> None:
    app.router.add_get(prefix + '/platform', get_platform, name='ajax_product_create_platform_get')
    app.router.add_get(prefix + '/game', get_game, name='ajax_product_create_game_get')
    app.router.add_get(prefix + '/service', get_service, name='ajax_product_create_service_get')
    app.router.add_get(prefix + '/advanced_form', get_advanced_form, name='ajax_product_create_advanced_form_get')
