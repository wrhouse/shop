import aiohttp.web

from shop.db.crud import (
    fn_get_product_option_value,
    fn_get_product,
    fn_get_currency,
    fn_get_shop,
    fn_get_service_children)
from shop.decorators import service_template
from shop.utils import json_formatter

get_list_product_json_formatter = json_formatter(['id', 'name', 'url', 'thumbnail_url', 'price_per_unit', 'stock'])
get_list_currency_json_formatter = json_formatter(['id', 'sign'])
get_list_shop_json_formatter = json_formatter(['id', 'name', 'url', 'image_url', 'rating'])


@service_template(only_json=True)
async def get_list(request: aiohttp.web.Request):
    engine = request.app['db']
    params = dict(request.rel_url.query)
    
    game_id = params.get('game_id') or None
    if game_id is not None:
        game_id = int(game_id)

    service_id = params.get('service') or None
    service_ids = None
    if service_id:
        service_ids = await fn_get_service_children(engine, service_id=int(service_id))
        
    options = params.get('options') or []
    
    product_ids = None
    if options:
        options = list(map(int, options.split(',')))
        product_option_values = await fn_get_product_option_value(engine, 
                                                                  game_option_value_ids=options)
        product_ids = list(set(product_option_value.product_id for product_option_value in product_option_values))
    
    limit = params.get('limit') or None
    if limit is not None:
        limit = int(limit)
    
    offset = params.get('offset') or None
    if offset is not None:
        offset = int(offset)
    
    products = await fn_get_product(engine,
                                    game_id=game_id,
                                    product_ids=product_ids,
                                    service_ids=service_ids,
                                    limit=limit,
                                    offset=offset)
    
    formatted_products = []
    for product in products:  # TODO: OPTIMIZE ME PLEASE
        currency = (await fn_get_currency(engine, currency_id=product.currency_id))[0]
        shop = (await fn_get_shop(engine, shop_id=product.shop_id))[0]

        formatted_products.append(get_list_product_json_formatter(product))
        formatted_products[-1]['currency'] = get_list_currency_json_formatter(currency)
        formatted_products[-1]['shop'] = get_list_shop_json_formatter(shop)

    return formatted_products
