from typing import Optional

import aiohttp.web

from shop.controllers.templates.ajax.product.create import routes as create_urls
from shop.utils.urls import include_urls

from .views import (
    get_list
)


def init_routes(app: aiohttp.web.Application,
                prefix: Optional[str] = None) -> None:
    include_urls(app, prefix + '/create', create_urls)

    app.router.add_get(prefix + '/list', get_list, name='ajax_product_list_get')
