from collections import defaultdict
from pprint import pprint
from typing import List

from aiohttp import web
from aiopg.sa import Engine
from sqlalchemy import select, func

from shop.db.models import Service, Product
from shop.utils.responses import success_response


async def get_counts(engine: Engine, *, game_id: int, parent_id=None) -> List[dict]:
    if not parent_id:
        parent_id = 0
    # TODO: NEED TO SEE WHAT ELSE WE CAN DO!!!
    # Nested services
    first_services_select = select([Service.id, Service.parent_id, Service.icon_url, Service.name], Service.parent_id == parent_id)
    async with engine.acquire() as db_connection:
        cursor = await db_connection.execute(first_services_select)
        target_services = await cursor.fetchall()
    all_services = []
    nested_services = target_services
    # Finding all services children.
    while nested_services:
        nested_services_select = select([Service.id,
                                         Service.parent_id,
                                         Service.icon_url,
                                         Service.name],
                                        Service.parent_id.in_(map(lambda s: s.id, nested_services)))
        async with engine.acquire() as db_connection:
            cursor = await db_connection.execute(nested_services_select)
            all_services.extend(nested_services)
            nested_services = await cursor.fetchall()
    # All products linked with selected services.
    product_select = select([
        func.count(Product.id).label('count'),
        Product.service_id.label('service_id')
    ], use_labels=True
    ).where(
        Product.game_id == game_id
    ).where(
        Product.service_id.in_(map(lambda x: x.id, all_services))
    ).group_by(
        Product.service_id
    )
    async with engine.acquire() as db_connection:
        cursor = await db_connection.execute(product_select)
        target_products = await cursor.fetchall()
    # Dict of services grouped by parent_id.
    counted_services = defaultdict(list)
    for service in all_services:
        service = dict(service)
        service_products = list(filter(lambda product: product.service_id == service['id'], target_products))
        service_count = 0
        if service_products:
            count = service_products[0]
            service_count = count.count
        service.update({'count': service_count, 'children': False})
        counted_services[service['parent_id']].append(service)
    # Folding services back
    target_services = counted_services.pop(parent_id)
    # Function to count all children of service
    # Also adds flag children to True if service has children

    def count_children(service: dict) -> int:
        nested_services = counted_services.get(service['id'])
        if not nested_services:
            return service['count']
        else:
            service['children'] = True
            return sum(map(count_children, nested_services))

    count_result = []
    for service in target_services:
        service = dict(service)
        service['count'] = count_children(service)
        count_result.append(service)

    return count_result


async def count_services(request: web.Request):
    """
    TODO: swagger
    params:
    -  game_id
    -  parent_id
    """
    params = dict(request.rel_url.query)
    game_id = params.get('game_id')
    parent_id = params.get('parent_id') or None
    if parent_id:
        parent_id = int(parent_id)
    engine = request.app['db']
    counts = await get_counts(engine, game_id=game_id, parent_id=parent_id)
    return success_response(counts)
