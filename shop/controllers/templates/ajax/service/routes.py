from typing import Optional

import aiohttp.web

from .views import count_services


def init_routes(app: aiohttp.web.Application,
                prefix: Optional[str] = None) -> None:
    app.router.add_get(prefix + '/counter', count_services, name='ajax_service_counter')