from typing import Optional

import aiohttp.web

from shop.utils.urls import include_urls
from .game import routes as game_urls
from .product import routes as product_urls
from .service import routes as service_urls
from .store import routes as store_urls


def init_routes(app: aiohttp.web.Application,
                prefix: Optional[str] = None) -> None:
    include_urls(app, prefix + '/game', game_urls)
    include_urls(app, prefix + '/product', product_urls)
    include_urls(app, prefix + '/service', service_urls)
    include_urls(app, prefix + '/store', store_urls)
