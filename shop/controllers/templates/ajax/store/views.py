import aiohttp.web

from shop.db.crud import (
    fn_create_rel_user_shop,
    fn_delete_rel_user_shop
)
from shop.decorators import authorized, service_template
from shop.responses import MissingParameter


@authorized(required=True)
@service_template(only_json=True)
async def follow(request: aiohttp.web.Request):
    engine = request.app['db']
    request_data = await request.post()
    user_id = request['user_id']
    shop_id = request_data.get('shop_id')

    if shop_id is None:
        raise MissingParameter('shop_id')

    await fn_create_rel_user_shop(engine, user_id=user_id, shop_id=int(shop_id))


@authorized(required=True)
@service_template(only_json=True)
async def unfollow(request: aiohttp.web.Request):
    engine = request.app['db']
    request_data = await request.post()
    user_id = request['user_id']
    shop_id = request_data.get('shop_id')

    if shop_id is None:
        raise MissingParameter('shop_id')

    await fn_delete_rel_user_shop(engine, user_id=user_id, shop_id=int(shop_id))
