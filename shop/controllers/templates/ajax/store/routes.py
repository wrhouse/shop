from typing import Optional

import aiohttp.web

from .views import (
    follow,
    unfollow
)


def init_routes(app: aiohttp.web.Application,
                prefix: Optional[str] = None) -> None:
    app.router.add_post(prefix + '/follow', follow, name='ajax_store_follow_post')
    app.router.add_post(prefix + '/unfollow', unfollow, name='ajax_store_unfollow_post')