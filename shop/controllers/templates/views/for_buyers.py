import aiohttp.web

from shop.decorators import service_template


@service_template('pages/for_buyers.jinja2', only_template=True)
async def for_buyers(request: aiohttp.web.Request):
    return {
        'title': 'title:for_buyers',
        'breadcrumbs': []
    }
