import aiohttp.web
import aiohttp_jinja2

from shop.decorators import lazy_authorized, mixin_user_info


@lazy_authorized(required=False)
@mixin_user_info('nickname', 'image_small')
@aiohttp_jinja2.template('pages/index.jinja2')
async def index(request: aiohttp.web.Request):
    return {}
