import aiohttp.web
import aiohttp_jinja2

from shop.db.crud.service import fn_get_service
from shop.decorators import lazy_authorized, mixin_user_info


@lazy_authorized(required=False)
@mixin_user_info('nickname', 'image_small')
async def games(request: aiohttp.web.Request):
    if 'platform' in request.rel_url.query:
        engine = request.app['db']
        platform_name = request.rel_url.query['platform']

        services = await fn_get_service(engine, parent_id=0, extended=True)
        return aiohttp_jinja2.render_template(template_name='pages/game/games.jinja2',
                                              request=request,
                                              context={
                                                  'services': services,
                                                  'title': platform_name,
                                                  'breadcrumbs': []
                                              })
    else:
        return aiohttp_jinja2.render_template(template_name='pages/game/all_games.jinja2',
                                              request=request,
                                              context={})