import aiohttp.web

from shop.decorators import service_template


@service_template('pages/about_us.jinja2', only_template=True)
async def about_us(request: aiohttp.web.Request):
    return {
        'title': 'title:about_us',
        'breadcrumbs': []
    }
