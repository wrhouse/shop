import aiohttp.web

from shop.db.crud import (
    fn_get_currency,
    fn_get_game_option,
    fn_get_platform,
    fn_get_product,
    fn_get_product_image,
    fn_count_rel_user_shop,
    fn_get_service,
    fn_get_service_hierarchy,
    fn_get_shop
)
from shop.decorators import lazy_authorized, mixin_user_info, identify_game, service_template
from shop.settings import Settings

settings = Settings()


@lazy_authorized(required=False)
@mixin_user_info('nickname', 'image_small')
@identify_game(fields=['id', 'url', 'name'])
@service_template('pages/trades.jinja2', only_template=True)
async def game_trades(request: aiohttp.web.Request):
    game = request['game']
    engine = request.app['db']

    filters = await fn_get_game_option(engine,
                                       game_id=game.id,
                                       extended=True)
    query_params = dict(request.rel_url.query)
    initial_service = None
    if 'service' in query_params:
        initial_service = (await fn_get_service(engine, service_id=query_params.get('service')))[0]

    return {
        'game': game,
        'filters': filters,
        'query_params': query_params,
        'initial_service': initial_service,

        'title': game.name,
        'breadcrumbs': []
    }


@lazy_authorized(required=False)
@mixin_user_info('nickname', 'image_small')
@identify_game(fields=['id', 'name', 'url'])
@service_template('pages/product/show.jinja2', only_template=True)
async def get_trade(request: aiohttp.web.Request):
    product_id = request.match_info['product_id']
    game = request['game']
    engine = request.app['db']

    product = (await fn_get_product(engine, product_ids=[product_id], extended=True))[0]
    currency = (await fn_get_currency(engine, currency_id=product.product.currency_id))[0]
    shop = (await fn_get_shop(engine, shop_id=product.product.shop_id))[0]
    service_path = await fn_get_service_hierarchy(engine=engine, service_id=product.product.service_id)
    platform = (await fn_get_platform(engine, platform_id=product.product.platform_id))[0]
    product_images = (await fn_get_product_image(engine, product_id=product_id))

    is_follower = bool(await fn_count_rel_user_shop(engine, user_id=request['user_id'], shop_id=shop.id))

    games_platform_url = request.app.router['games'].url_for().with_query(**{'platform': platform.name})
    game_url = request.app.router['game_trades'].url_for(**{'game_url': game.url})

    return {
        'game': game,
        'shop': shop,
        'product': product,
        'currency': currency,
        'platform': platform,
        'product_images': product_images,
        'service_path': service_path,

        'is_follower': is_follower,

        'title': product.product.name,
        'breadcrumbs': [
            (games_platform_url, platform.name),
            (game_url, game.name)
        ]
    }
