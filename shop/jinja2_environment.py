from typing import Dict, List, Callable

import aiohttp.web

from shop.settings import Settings
from shop.utils.context_processors import (
    ext_url_func,
    translate_func_ctx
)

settings = Settings()


def is_index(path: str) -> bool:
    return not bool(path.strip('/'))


def append(value: str, array: List[str]) -> str:
    array.append(value)
    return ''


def setup_jinja2_filters() -> Dict[str, Callable]:
    return {
        'is_index': is_index,
        'append': append
    }


async def with_request_context(request: aiohttp.web.Request):
    return {'request': request}


async def with_imports_context(request: aiohttp.web.Request):
    return {'js_imports': []}


async def with_localization_context(request: aiohttp.web.Request):
    translate_func = translate_func_ctx(request)
    return {'_': translate_func}


async def with_setting_ctx(request: aiohttp.web.Request):
    return {'settings': settings}


async def with_ext_url_context(request: aiohttp.web.Request):
    return {'ext_url': ext_url_func}


def setup_jinja2_context_processors() -> List[Callable]:
    return [
        with_localization_context,
        with_ext_url_context,
        with_request_context,
        with_imports_context,
        with_setting_ctx
    ]


__all__ = [
    'setup_jinja2_filters',
    'setup_jinja2_context_processors'
]
