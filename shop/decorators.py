import asyncio
import functools
from typing import Any, List, Optional

import aiohttp.abc
import aiohttp.web
import aiohttp_jinja2
import ujson as json
from account import AccountClient
from account.errors import AccessTokenExpired, AccessTokenDeleted
from account.models import UserFields
from sqlalchemy import select, and_

import shop.responses as responses
from shop.db.models import Shop, Game, ShopMemberPermission
from shop.utils.security import decode_payload, token_is_expired


def lazy_authorized(required: bool = False):
    def wrapper(handler):
        @functools.wraps(handler)
        async def wrapped(request: aiohttp.web.Request):
            settings = request.app['settings']
            access_token = request.cookies.get('access_token')
            is_ajax = bool(int(request.rel_url.query.get('is_ajax') or '0'))

            if access_token is not None:
                payload = decode_payload(token=access_token, secret_key=settings.access_token_secret_key)
                is_expired = token_is_expired(payload=payload, expire_in=settings.access_token_expire_in)
                if not is_expired:
                    request['user_id'] = payload['user_id']
                    return await handler(request)

            if not required:
                request['user_id'] = None
                return await handler(request)

            if is_ajax:
                raise responses.AccessDeniedError() from None
            else:
                raise responses.ServiceRedirect('profile', 'login')

        return wrapped

    return wrapper


def authorized(required: bool = False):
    def wrapper(handler):
        @lazy_authorized(required=required)
        @functools.wraps(handler)
        async def wrapped(request: aiohttp.web.Request):
            if not required and request['user_id'] is None:
                return await handler(request)

            account_service: AccountClient = request.app['account']
            access_token = request.cookies.get('access_token')
            is_ajax = bool(int(request.rel_url.query.get('is_ajax') or '0'))

            try:
                await account_service.tokens.validate(access_token=access_token)
                return await handler(request)
            except AccessTokenExpired:
                if is_ajax:
                    raise responses.AccessTokenExpired() from None
            except AccessTokenDeleted:
                if is_ajax:
                    raise responses.AccessTokenDeleted() from None
            raise responses.ServiceRedirect('profile', 'login')

        return wrapped
    return wrapper


def identify_store(fields: Optional[List[str]] = None):
    if fields is None:
        fields = ['id']
    selected_fields = [Shop.__table__.c[field] for field in fields]

    def wrapper(handler):
        @functools.wraps(handler)
        async def wrapped(request: aiohttp.web.Request):
            store_url = request.match_info['store_url']
            select_query = select(selected_fields).where(Shop.url == store_url)

            async with request.app['db'].acquire() as connection:
                cursor = await connection.execute(select_query)
                store = await cursor.fetchone()
                if store is not None:
                    request['store'] = store
                    return await handler(request)

            raise aiohttp.web.HTTPNotFound() from None

        return wrapped
    return wrapper


def identify_game(fields: Optional[List[str]] = None):
    if fields is None:
        fields = ['id']
    selected_fields = [Game.__table__.c[field] for field in fields]

    def wrapper(handler):
        @functools.wraps(handler)
        async def wrapped(request: aiohttp.web.Request):
            game_url = request.match_info['game_url']
            select_query = select(selected_fields).where(Game.url == game_url)

            async with request.app['db'].acquire() as connection:
                cursor = await connection.execute(select_query)
                game = await cursor.fetchone()
                if game is not None:
                    request['game'] = game
                    return await handler(request)

            raise aiohttp.web.HTTPNotFound() from None

        return wrapped
    return wrapper


def identify_product_by_game(handler):
    @identify_game()
    @functools.wraps(handler)
    async def wrapped(request: aiohttp.web.Request):
        pass

    return wrapped


def identify_product_by_store(handler):
    @identify_store()
    @functools.wraps(handler)
    async def wrapped(request: aiohttp.web.Request):
        pass

    return wrapped


def check_store_member_permissions(*permissions: List[str]):
    def wrapper(handler):
        @functools.wraps(handler)
        async def wrapped(request: aiohttp.web.Request):
            store = request['store']

            async with request.app['db'].acquire() as connection:
                is_owner_query = (select([Shop.id])
                                  .where(and_(Shop.id == store.id,
                                              Shop.owner_id == request['user_id'])))
                cursor = await connection.execute(is_owner_query)
                is_owner = await cursor.fetchone()
                if is_owner is not None:
                    return await handler(request)

                for permission_id in permissions:
                    member_permission_exists_query = (select([ShopMemberPermission.shop_id])
                                                      .where(and_(ShopMemberPermission.shop_id == store.id,
                                                                  ShopMemberPermission.user_id == request['user_id'],
                                                                  ShopMemberPermission.permission_id == permission_id)))
                    cursor = await connection.execute(member_permission_exists_query)
                    member_permission_exists = await cursor.fetchone()
                    if member_permission_exists is not None:
                        return await handler(request)

            raise responses.AccessDeniedError(permissions) from None

        return wrapped
    return wrapper


def mixin_user_info(*fields: List[str]):
    def wrapper(handler):
        @functools.wraps(handler)
        async def wrapped(request: aiohttp.web.Request):
            if request['user_id'] is None:
                request['user'] = None
                return await handler(request)

            account_service: AccountClient = request.app['account']
            user_fields = [UserFields(field) for field in fields]

            request['user'] = await account_service.users.get_user_info(user_id=request['user_id'],
                                                                        service_id='profile',
                                                                        fields=user_fields)
            return await handler(request)

        return wrapped
    return wrapper


def service_template(template_name: Optional[str] = None,
                     only_template: bool = False,
                     only_json: bool = False,
                     *,
                     app_key: str = aiohttp_jinja2.APP_KEY,
                     encoding: str = 'utf-8',
                     status: int = 200) -> Any:
    if only_json and only_template:
        raise ValueError('only_json and only_template')  # TODO: не могут быть истиными одновременно
    if template_name is None and not only_json:
        raise ValueError('template_name')  # TODO: может быть None только если only_json истинно

    def wrapper(func: Any) -> Any:
        @functools.wraps(func)
        async def wrapped(*args: Any) -> aiohttp.web.StreamResponse:
            if asyncio.iscoroutinefunction(func):
                handler = func
            else:
                handler = asyncio.coroutine(func)
            context = await handler(*args)

            if isinstance(context, aiohttp.web.StreamResponse):
                return context

            if isinstance(args[0], aiohttp.abc.AbstractView):
                request = args[0].request
            else:
                request = args[-1]

            is_ajax = bool(int(request.rel_url.query.get('is_ajax') or 0))
            if (is_ajax and not only_template) or only_json:
                json_data = dict(success=True)
                if context is not None:
                    json_data['data'] = context
                return aiohttp.web.json_response(data=json_data,
                                                 dumps=json.dumps,
                                                 status=status)
            else:
                context = context or {}
                return aiohttp_jinja2.render_template(template_name,
                                                      request,
                                                      context,
                                                      app_key=app_key,
                                                      encoding=encoding,
                                                      status=status)
        return wrapped
    return wrapper
