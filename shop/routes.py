import os

import aiohttp.web

from shop.controllers import routes as urls
from shop.settings import Settings
from shop.utils.urls import include_urls, include_static_path


def init_routes(app: aiohttp.web.Application) -> None:
    include_urls(app, None, urls)

    settings = Settings()
    include_static_path(app, settings.path_to_static_files, '/css')
    include_static_path(app, settings.path_to_static_files, '/js')
    include_static_path(app, settings.path_to_static_files, '/images')
