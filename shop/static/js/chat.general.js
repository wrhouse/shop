$(document).ready(function () {
    let restBaseEndpoint = 'https://chat.gaming-warehouse.com';
    let wsBaseEndpoint = 'wss://chat.gaming-warehouse.com';
    const accessToken = getCookie('access_token');

    let ws = new WebSocket(`${wsBaseEndpoint}/api/ws/general/shop`);
    let currentChannelId = null;

    let startMessageId = 0;
    const count = 200;  // TODO: change to 20
    const rev = 1;

    let messageBoxIsBottom = true;

    const channelInfoDesktopTemplate = (channel) => `
        <a href="#message-box" class="channel-item" id="channel:${channel.channel_id}">
            <div class="item">
                <img src="${channel.image}" />
                <div class="info">
                    <div class="name">${channel.name}</div>
                    <p>${channel.description}</p>
                </div>
            </div>
        </a>
    `;

    const channelInfoMobileTemplate = (channel) => `
        <a href="#message-box" class="channel-item" id="channel-mobile:${channel.channel_id}">
            <p class="item">${channel.name}</p>
        </a>
    `;

    const messageTemplate = (message) => `
        <div class="post">
            <div class="ico no_mob">
                <img src="https://profile.gaming-warehouse.com/api/v1/account/picture/${message.from_id}" alt="" />
            </div>
            <div class="mess">
                <p>${message.text}</p>
                <div class="menu"></div>
            </div>
        </div>
    `;

    function messageBoxToBottom() {
        let messageBox = $("#message-box");
        messageBox.scrollTop(messageBox.prop("scrollHeight"));
    }

    ws.onmessage = function (event) {
        let message = JSON.parse(event.data);
        let messageBox = $("#message-box");
        messageBox.append(messageTemplate(message));
        if (messageBoxIsBottom) {
            messageBoxToBottom();
        }
    };

    function getCookie(cookieName) {
        var value = "; " + document.cookie;
        var parts = value.split("; " + cookieName + "=");
        if (parts.length == 2)
            return parts.pop().split(";").shift();
    }

    function loadHistory(channelId) {
        let historyEndpoint = `${restBaseEndpoint}/api/rest/general/shop/channel/${channelId}`;
        $.ajax({
            type: 'GET',
            url: historyEndpoint,
            data: JSON.stringify({
                start_message_id: startMessageId,
                count: count,
                rev: rev
            }),
            success: function (json) {
                if (json.success) {
                    let messageBox = $('#message-box');
                    let messages = json.data.messages;
                    let oldStartMessageId = startMessageId;

                    messages.forEach(function (message) {
                        messageBox.prepend(messageTemplate(message));
                        startMessageId = message.message_id;
                    });

                    if (oldStartMessageId == 0) {
                        messageBoxToBottom();
                    }
                }
            }
        })
    }

    function changeChannel(channelId) {
        if (currentChannelId != null) {
            ws.send(JSON.stringify({
                command: 'unsubscribe',
                channel_id: currentChannelId
            }));
        }

        currentChannelId = Number(channelId);
        ws.send(JSON.stringify({
            command: 'subscribe',
            channel_id: currentChannelId
        }));

        $('#message-box').html('');
        startMessageId = 0;

        loadHistory(currentChannelId);
    }

    function loadChannelList() {
        let channelListEndpoint = `${restBaseEndpoint}/api/rest/general/shop/channel`;
        $.ajax({
            type: 'GET',
            url: channelListEndpoint,
            success: function (json) {
                if (json.success) {
                    let channelListBox = $('#channel-list-box');
                    let channelListBoxMobile = $('#channel-list-box-mobile');
                    let channels = json.data.channels;

                    channels.forEach(function (channel) {
                        channelListBox.append(channelInfoDesktopTemplate(channel));
                        channelListBoxMobile.append(channelInfoMobileTemplate(channel));
                    });

                    if (channels.length > 0) {
                        changeChannel(channels[0].channel_id);
                    }

                    $('.channel-item').on('click', function() {
                        let channelId = Number($(this).attr("id").split(':')[1]);
                        changeChannel(channelId);
                    });
                }
            }
        });
    }

    function scrollMessageBox(event) {
        let element = $(event.currentTarget);
        if ((element[0].scrollHeight - element.scrollTop()) == element.outerHeight()) {
            messageBoxIsBottom = true;
        } else {
            messageBoxIsBottom = false;
        }
    }

    $('#send-button').on('click', function () {
        if (accessToken) {
            let sendInput = $('#send-input');
            $.ajax({
                type: 'POST',
                url: `${restBaseEndpoint}/api/rest/general/shop/channel/${currentChannelId}?access_token=${accessToken}`,
                data: JSON.stringify({
                    'text': sendInput.val()
                }),
                success: function (json) {
                    if (json.success) {
                        sendInput.val('');
                    }
                }
            });
        }
    });

    $('#send-input').keyup(function (event) {
        if (event.which == 13) {
            $('#send-button').click();
        }
    });

    $('#message-box').bind('scroll', scrollMessageBox);

    ws.onopen = function(event) {
        loadChannelList();
    };
});