const gameItem = (game) => {
    let span_services = "";
    game.services.forEach((service) => {
        span_services += `<span class="service_select" redirect_url="/games/${game.url}?service=${service.id}">${service.name}</span>`
    });
    let ret = `
            <a href="/games/${game.url}" class="item">
                <img src="${aws_bucket}/${game.image_url}" alt="">
                <span class="only_mob name">${game.name}</span>
                <span class="no_mob more_box">
                    ${span_services}
                </span>
            </a>`;
    return ret;
};

let offset = 0;
let limit = 18;


function gatherFilters() {
    let params = {
        offset: offset,
        limit: limit
    };
    if (typeof target_platform_group !== 'undefined') {
        params.platform_group = target_platform_group;
    }
    let checked_services = $(".services input[type='checkbox']").filter((i, e) => e.checked).map((i, e) => $(e));
    let services = [];
    checked_services.each((i, e) => {
        services.push(Number(e.attr('service_id')));
    });
    if (services.length > 0) {
        params['services'] = services.join(',');
    }
    return params;
}

function load_mode_games(remove = false) {
    let params = gatherFilters();
    $.ajax({
        url: "/ajax/game/list",
        method: "GET",
        data: params
    }).done(function (response, status) {
        if (response.success) {
            offset += limit;
            if (response.data.length === 0) {
                $('.see_more_line').hide();
            }
            if (remove) {
                $(".catalog .item").remove();
            }
            response.data.forEach((item) => {
                console.log(item);
                $(gameItem(item)).insertBefore('.see_more_line');
            })
        }
    })

}

function reload_games() {
    offset = 0;
    load_mode_games(true);
}

function drop_filters() {
    let checked_services = $(".services input[type='checkbox']").filter((i, e) => e.checked).map((i, e) => $(e));
    checked_services.each((i, e) => {
        $(e).prop("checked", false);
    });
}

$(document).on('click', '.see_more', function (event) {
    load_mode_games()
});


$(document).ready(() => {
    $('.see_more').appear();
    $(document.body).on('appear', '.see_more', function (e, $affected) {
        load_mode_games();
    });
    $(".services input[type='checkbox']").on('click', event => {
        reload_games();
    });
    $(".reset").on('click', event => {
        drop_filters();
        reload_games();
    });
    $(document).on('click', '.service_select', event => {
        let target = $(event.target);
        window.location.replace(target.attr('redirect_url'))
    });
    reload_games();
});