const PLATFORM_LIST_ENDPOINT = '/ajax/product/create/platform';
const GAME_LIST_ENDPOINT = '/ajax/product/create/game';
const SERVICE_LIST_ENDPOINT = '/ajax/product/create/service';
const ADVANCED_FORM_ENDPOINT = '/ajax/product/create/advanced_form';
const BASE_SUBMIT_URL = window.location.pathname;

const productImageTemplate = ({src} = {}) => {
    return `
        <div class="add_new fill">
            <img class="product_image" src="${src}" />
            <div class="delete_image">&times;</div>
            <input type="hidden" name="product[images][]" value="${src}" />
        </div>
    `;
};

function createProduct(requestData) {
    $.ajax({
        url: BASE_SUBMIT_URL,
        type: 'POST',
        data: JSON.stringify(requestData),
        success: function(response) {
            if (response.success) {
                window.location.href = '/';  // TODO: Redirect to manage listings page
            } else {
                console.error(response.responseText);
            }
        }
    });
}

function collectGeneralInfo() {
    let data = {
        game_id: Number($('.select_game').val()),
        service_id: Number($('.select_service').last().val()),
        platform_id: Number($('.select_platform').val()),
        game_option_value_ids: Array.from($('.select_game_option')).map((elem) => Number($(elem).val())),
        name: $('.product_name').val(),
        description: $('.product_description').val(),
        images: Array.from($('.product_image')).map((elem) => $(elem).attr('src')),
        currency_id: Number($('.select_currency').val()),
        stock: Number($('.product_stock').val()),
        price_per_unit: Number($('.product_price_per_unit').val().replace(',', '.')) * 100,
        min_unit_per_order: Number($('.product_min_unit_per_order').val()),
        online_hrs: Number($('.select_online_hrs').val()),
        offline_hrs: Number($('.select_offline_hrs').val())
    };

    let selectedInsurances = Array.from($('.select_insurance')).
                             filter((elem, index) => $(elem).is(':checked'));
    if (selectedInsurances.length > 0) {
        data.insurance_id = Number($(selectedInsurances[0]).val());
    }

    let selectedDurations = Array.from($('.select_duration')).
                            filter((elem, index) => $(elem).is(':checked'));
    if (selectedDurations.length > 0) {
        data.duration_id = Number($(selectedDurations[0]).val());
    }

    data.delivery_option_ids = Array.from($('.select_delivery_option')).
                               filter((elem, index) => $(elem).is(':checked')).
                               map((elem, index) => Number($(elem).attr('val')));

    return data;
}

function checkTermsOfService() {
    return $('#terms-of-service').is(':checked');
}

function resetGameSelector() {
    let selectGameElement = $('.select_game');
    let placeholderOption = $(selectGameElement).children()[0];
    $(selectGameElement).html(placeholderOption);
    $(selectGameElement).val('');

    let placeholderText = $($(selectGameElement).children()[0]).html();
    $(selectGameElement).prev().text(placeholderText);

    $('#form').html('');
}

function resetServiceSelector() {
    Array.from($('.service-box').children().slice(1)).forEach((service) => $(service).remove())

    let selectServiceElement = $('.select_service');
    let placeholderOption = $(selectServiceElement).children()[0];
    $(selectServiceElement).html(placeholderOption);
    $(selectServiceElement).val('');

    let placeholderText = $($(selectServiceElement).children()[0]).html();
    $(selectServiceElement).prev().text(placeholderText);

    $('#form').html('');
}

$(document).ready(function() {
    $.ajax({
        method: 'GET',
        url: PLATFORM_LIST_ENDPOINT,
        success: function(data) {
            if (data.success) {
                let platforms = data.data;
                let selectPlatformElement = $('.select_platform');
                platforms.forEach(function(platform) {
                    $(selectPlatformElement).append(optionTemplate(platform.id, platform.name));
                });
            }
        }
    });

    /* После выбора платформы подгружается список игр, доступных для выбранной платформы */
    $(document).on('change', '.select_platform', function(event) {
        resetGameSelector();
        resetServiceSelector();

        let platformId = $(event.target).val();
        if (platformId.length > 0) {
            platformId = Number(platformId);

            $.ajax({
                method: 'GET',
                url: GAME_LIST_ENDPOINT,
                data: {
                    platform_id: platformId
                },
                success: function(data) {
                    if (data.success) {
                        let games = data.data;
                        let selectGameElement = $('.select_game');
                        games.forEach(function(game) {
                            $(selectGameElement).append(optionTemplate(game.id, game.name));
                        });
                    }
                }
            });
        }
    });

    /* После выбора игры подгружается список сервисов, доступных для выбранной игры */
    $(document).on('change', '.select_game', function(event) {
        resetServiceSelector();

        let gameId = $(event.target).val();
        if (gameId.length > 0) {
            gameId = Number(gameId);

            $.ajax({
                method: 'GET',
                url: SERVICE_LIST_ENDPOINT,
                data: {
                    game_id: gameId,
                    parent_id: 0
                },
                success: function(data) {
                    if (data.success) {
                        let services = data.data;
                        let selectServiceElement = $('.select_service');
                        services.forEach(function(service) {
                            $(selectServiceElement).append(optionTemplate(service.id, service.name));
                        });
                    }
                }
            });
        }
    });

    /* После выбора сервиса подгружается оставшаяся форма добавления товара */
    $(document).on('change', '.select_service', function(event) {
        $('#form').html('');

        let currentParent = $($(event.target).parent()).parent();
        let startIndex = $('.service-box').children().index($(currentParent)) + 1;
        Array.from($('.service-box').children().slice(startIndex)).forEach((serviceSelector) => $(serviceSelector).remove());

        let gameId = $('.select_game').val();
        let serviceId = $(event.target).val();

        if (gameId.length > 0 && serviceId.length > 0) {
            gameId = Number(gameId);
            serviceId = Number(serviceId);

            $.ajax({
                method: 'GET',
                url: SERVICE_LIST_ENDPOINT,
                data: {
                    game_id: gameId,
                    parent_id: serviceId
                },
                success: function(data) {
                    if (data.success) {
                        let services = data.data;
                        if (services.length > 0) {
                            let placeholder = $($('.select_service').children()[0]).html();
                            $('.service-box').append(selectListTemplate('', 'select_service', placeholder, services));
                        } else {
                            $.ajax({
                                method: 'GET',
                                url: ADVANCED_FORM_ENDPOINT,
                                data: {
                                    game_id: gameId,
                                    service_id: serviceId
                                },
                                success: function(body) {
                                    $('#form').html(body);
                                }
                            });
                        }
                    }
                }
            });
        }
    });

    $(document).on('change', '#image-select', function(event) {
        let productImageElement = document.getElementById('image-select');
        Array.from(productImageElement.files).forEach(function(file) {
            let fileReader = new FileReader();
            fileReader.readAsDataURL(file);
            fileReader.onloadend = function(loadEvent) {
                let imageBase64 = loadEvent.target.result;
                let productImage = productImageTemplate({
                    src: imageBase64
                });
                $(productImage).insertBefore(".add_button");
            };
        });
        $(productImageElement).val('');
    });

    $(document).on('click', '.delete_image', function(event) {
        $(this).parent().remove();
    });

    $(document).on('click', '#submit', function(event) {
        if (checkTermsOfService()) {
            let data = collectGeneralInfo();
            createProduct(data);
        } else {
            alert('Press to Terms of Service'); // TODO:
        }
    });
});