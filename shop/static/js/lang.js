function setCookie(name, value) {
    document.cookie = name + "=" + (value || "") + "; path=/";
}

$(document).ready(function () {
    $('.lang-link').on('click', function () {
        let lang_code = $(this).html().toLowerCase();
        setCookie('lang_code', lang_code);
        location.reload();
    });
});