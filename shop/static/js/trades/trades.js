let offset = 0;
let limit = 10;


function gatherFilters() {
    let filters = Object.assign({}, default_trades_filters);
    filters['offset'] = offset;
    filters['limit'] = limit;
    filters['service'] = serviceParams.service;
    let game_options = [];
    $(".filter_block :input").each((index, input) => {
        let value = $(input).val();
        if (null !== value) {
            game_options.push(value);
        }
    });
    filters['options'] = game_options.join(',');
    if (history.replaceState) {
        let query = {};
        if (filters['service'] && filters['service'] !== 0) {
            query.service = filters['service'];
        }
        if (filters['options']) {
            query.options = filters['options'];
        }
        let query_str = Object.keys(query)
            .map(k => encodeURIComponent(k) + '=' + encodeURIComponent(query[k]))
            .join('&');
        let new_url = `${window.location.protocol}//${window.location.host}${window.location.pathname}?${query_str}`;
        window.history.replaceState({path: new_url}, '', new_url);
    }
    return filters;
}

function render_stars(count) {
    let starsStr = '';
    for (let i = 0; i < 5; i++) {
        if (i < Math.floor(count)) {
            starsStr += '<div class="active"></div>';
        } else {
            starsStr += '<div></div>'
        }
    }
    return starsStr;
}


const trade_lot = (product) => `
<div class="lot" product_url="/games/${game.game_url}/${product.url}-${product.id}">
                <div class="group_1">
                    <div class="name_lot">
                        <img class="pointer link_lot" src="${aws_bucket}/${product.thumbnail_url}" alt="">
                        <div class="text">
                            <p class="name pointer link_lot">${product.name}</p>
                        </div>
                    </div>
                    <div class="seller">
                        <div class="ico">
                            <img  src="${aws_bucket}/${product.shop.image_url}" alt="">
                            <span class="online"></span>
                        </div>
                        <div>
                            <p class="name pointer link_lot">${product.shop.name}</p>
                            <div class="stars">
                                ${render_stars(product.shop.rating)}
                                <p>${product.shop.rating}</p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="group_2 col3">
                    <div class="code">${product.shop.id}</div>
                    <div class="count" data-title="количество:">
                        ${product.stock}
                    </div>
                    <div class="price">
                        <p>${product.price_per_unit / 100} <span>${product.currency.sign}</span></p>
                        <a class="btn">Buy now</a>
                    </div>
                </div>
            </div>
`;

function get_next_trades(remove = false) {
    let params = gatherFilters();
    $.ajax({
        url: trades_url,
        method: 'GET',
        data: params
    }).done(function (response, status) {
        if (!response.success) {
            return;
        }
        offset += limit;
        if (response.data.length === 0) {
            $('.see_more_line').hide();
        }
        if (remove) {
            $(".lots_block .lot").remove()
        }
        response.data.forEach((item) => {
            $(trade_lot(item)).insertBefore($('.see_more_line'), null);
        })
    })
}

function full_update_trades() {
    offset = 0;
    get_next_trades(true);
}

function reset_filters() {
    let custom_options = null;
    if (queryString.hasOwnProperty('options')) {
        custom_options = queryString.options.split(',');
        delete queryString.options;
    }
    $('select').each((index, sel) => {
        let target = $(sel);
        console.log(target.val());
        target.val(null);
        $(target.prev()).html($(target.find('option:first')).text());
        if (null !== custom_options) {
            custom_options.forEach((option_value) => {
                let opt = $(`option[value="${option_value}"]`);
                if (opt.length !== 0) {
                    opt.attr('selected', 'selected');
                    $(target.prev()).html(opt.text());
                }
            });
        }
    });
    $(".trade_service.active").each((index, service) => {
        $(service).removeClass('active');
    });
}

$(document).ready(_ => {
    $('.see_more_btn').appear();
    $(document.body).on('appear', '.see_more_btn', function (e, $affected) {
        get_next_trades();
    });
    reset_filters();
    $(document).on('change', '.filter_block :input', _ => {
        full_update_trades();
    });
    $(document).on('click', '.reset', _ => {
        reset_filters();
        full_update_trades();
    });
    $(document).on('click', '.see_more_btn', _ => {
        get_next_trades();
    });
    $(document).on('click', '.link_lot', event => {
        let target = $(event.target);
        let lot = target.closest('.lot');
        window.location.assign($(lot).attr('product_url'));
    });
    full_update_trades();
});
