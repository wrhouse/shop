let old_parents = [];
let current_parent = 0;
let select_all_id = 0;

const service_item = ({count, id, name, parent_id, children, icon_url}, active) => `
<a service_id="${id}" parent_id="${parent_id}" has-children="${children}" class="item trade_service ${active ? 'active' : ''}">
    <span class="count">${count}</span>
    <img src="${aws_bucket}/${icon_url}" alt="">
    <span class="name">${name}</span>
</a>`;

function gatherServiceParams() {
    if (queryString.hasOwnProperty('service')) {
        serviceParams.service = Number(queryString.service);
        delete queryString.services;
    }
    return serviceParams
}

function get_new_services(render_back_button = false, active_service = 0) {
    let params = gatherServiceParams();
    $.ajax({
        url: services_url,
        method: 'GET',
        data: params
    }).done(function (response, status) {
        if (!response.success) {
            return;
        }
        $(".slider.mini .owl-carousel").remove();
        $('.slider.mini').append($('<div class="owl-carousel trades_counts"></div>'));
        if (render_back_button) {
            $(".owl-carousel.trades_counts").append(
                `<a class="item back_button">
                    <img src="/images/icons/back-arrow.svg" alt="">
                    <span class="name">${backText}</span>
                </a>`);
            $(".owl-carousel.trades_counts").append(
                `<a class="item active select_all_btn">
                    <img src="/images/icons/shield.svg" alt="">
                    <span class="name">${selectAllText}</span>
                </a>`);
        }
        response.data.forEach((item) => {
            console.log(item);
            let item_active = false;
            if (item.id === active_service){
                $(".select_all_btn").removeClass('active');
                item_active = true;
            }
            $(".owl-carousel.trades_counts").append(service_item(item, item_active));
        });
        $(".slider.mini .owl-carousel").owlCarousel({
            center: !0,
            items: response.data.length,
            loop: !0,
            stagePadding: 20,
            margin: 0,
            dots: !0,
            nav: !0,
            navText: [""],
            responsive: {
                425: {items: 3},
                670: {items: 4, loop: !1, stagePadding: 0, center: !1, mouseDrag: !1, touchDrag: !1}
            }
        });
    })
}

$(document).ready((event) => {
    let render_button = false;
    if (serviceParams.parent_id !== null) {
        render_button = true;
        old_parents = [0];
    }
    get_new_services(render_button, serviceParams.service);

    $(document).on('click', '.trade_service', (event) => {
        event.preventDefault();
        let target = $($(event.target).closest('a'));
        serviceParams.service = Number(target.attr('service_id'));
        if (target.attr('has-children') === "true") {
            current_parent = target.attr('parent_id');
            old_parents.push(current_parent);
            serviceParams.parent_id = target.attr('service_id');
            select_all_id = target.attr('service_id');
            get_new_services(true);
            full_update_trades();
        } else {
            target.toggleClass('active');
            if (!target.hasClass('active')) {
                $(".select_all_btn").addClass('active');
                serviceParams.service = select_all_id;
            } else {
                $('.trade_service').removeClass('active');
                target.addClass('active');
                $(".select_all_btn").removeClass('active');
                console.log(old_parents);
            }
            full_update_trades();
        }
    });
    $(document).on('click', '.select_all_btn', (_) => {
        if (!$(".select_all_btn").hasClass('active')) {
            $('.select_all_btn').addClass('active');
            serviceParams.service = select_all_id;
            $('.trade_service').removeClass('active');
            full_update_trades();
        }
    });
    $(document).on('click', '.back_button', (_) => {
        let old_id = old_parents.pop();
        let render_button = true;
        serviceParams.parent_id = old_id;
        select_all_id = old_id;
        if (old_parents.length === 0) {
            render_button = false;
        }
        serviceParams.service = Number(old_id);
        get_new_services(render_button);
        full_update_trades(serviceParams);
    });
});