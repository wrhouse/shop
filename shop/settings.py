import os

from pydantic import BaseSettings


class Settings(BaseSettings):
    @property
    def service_host(self) -> str:
        return os.environ["SERVICE_HOST"]

    @property
    def service_port(self) -> int:
        return int(os.environ["SERVICE_PORT"])

    @property
    def environment(self) -> str:
        return os.environ["ENVIRONMENT"]

    @property
    def service_domain(self) -> str:
        return os.environ["SERVICE_DOMAIN"]

    @property
    def database_host(self) -> str:
        return os.environ["DATABASE_HOST"]

    @property
    def database_port(self) -> int:
        return int(os.environ["DATABASE_PORT"])

    @property
    def database_user(self) -> str:
        return os.environ["DATABASE_USER"]

    @property
    def database_password(self) -> str:
        return os.environ["DATABASE_PASSWORD"]

    @property
    def database_name(self) -> str:
        return os.environ["DATABASE_NAME"]

    @property
    def path_to_templates(self) -> str:
        return os.environ["PATH_TO_TEMPLATES"]

    @property
    def path_to_static_files(self) -> str:
        return os.environ["PATH_TO_STATIC_FILES"]

    @property
    def locale_update_time(self) -> int:
        return int(os.environ["LOCALE_UPDATE_TIME"])

    @property
    def account_service(self) -> str:
        return os.environ["ACCOUNT_SERVICE"]

    @property
    def access_token_expire_in(self) -> int:
        return int(os.environ["ACCESS_TOKEN_EXPIRE_IN"])

    @property
    def access_token_secret_key(self) -> str:
        return os.environ["ACCESS_TOKEN_SECRET_KEY"]

    @property
    def aws_access_key_id(self) -> str:
        return os.environ['AWS_ACCESS_KEY_ID']

    @property
    def aws_secret_access_key(self) -> str:
        return os.environ['AWS_SECRET_ACCESS_KEY']

    @property
    def aws_bucket(self) -> str:
        return os.environ['AWS_BUCKET']

    @property
    def product_image_size(self) -> (int, int):
        size = os.environ["PRODUCT_IMAGE_SIZE"]
        return tuple(map(int, size.split('*')))

    @property
    def product_thumbnail_size(self) -> (int, int):
        size = os.environ["PRODUCT_THUMBNAIL_SIZE"]
        return tuple(map(int, size.split('*')))

    @property
    def min_online_hrs(self) -> int:
        return int(os.environ["MIN_ONLINE_HRS"])

    @property
    def max_online_hrs(self) -> int:
        return int(os.environ["MAX_ONLINE_HRS"])

    @property
    def min_offline_hrs(self) -> int:
        return int(os.environ["MIN_OFFLINE_HRS"])

    @property
    def max_offline_hrs(self) -> int:
        return int(os.environ["MAX_OFFLINE_HRS"])
