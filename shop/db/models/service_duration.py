from sqlalchemy import Column, Integer, ForeignKey

from shop.migrations import Base
from .service import Service


class ServiceDuration(Base):
    __tablename__ = 'service_duration'

    id = Column(Integer(), primary_key=True)
    service_id = Column(Integer(), ForeignKey(Service.id, ondelete='CASCADE'), nullable=False)
    duration_days = Column(Integer(), nullable=False)
