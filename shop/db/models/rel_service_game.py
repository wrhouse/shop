from sqlalchemy import Column, Integer, ForeignKey, String
from sqlalchemy.sql import literal

from shop.migrations import Base
from .game import Game
from .service import Service


class RelServiceGame(Base):
    __tablename__ = 'rel_service_game'

    game_id = Column(Integer(), ForeignKey(Game.id, ondelete='CASCADE'), nullable=False, primary_key=True)
    service_id = Column(Integer(), ForeignKey(Service.id, ondelete='CASCADE'), nullable=False, primary_key=True)
    price_unit_placeholder = Column(String(length=16), nullable=False, server_default='')
    advanced_form_name = Column(String(length=32), nullable=False)
