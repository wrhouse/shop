from sqlalchemy import Column, Integer, ForeignKey

from shop.migrations import Base
from .service import Service


class ServiceInsurance(Base):
    __tablename__ = 'service_insurance'

    id = Column(Integer(), primary_key=True)
    service_id = Column(Integer(), ForeignKey(Service.id, ondelete='CASCADE'), nullable=False)
    insurance_days = Column(Integer(), nullable=False)
