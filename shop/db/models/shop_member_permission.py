from sqlalchemy import Column, Integer, String, ForeignKey, ForeignKeyConstraint, Index

from shop.migrations import Base
from .shop_member import ShopMember


class ShopMemberPermission(Base):
    __tablename__ = 'shop_member_permission'

    shop_id = Column(Integer(), nullable=False, primary_key=True)
    user_id = Column(Integer(), nullable=False, primary_key=True)
    permission_id = Column(String(length=32), nullable=False, primary_key=True)

    __table_args__ = (ForeignKeyConstraint((shop_id, user_id),
                                           (ShopMember.shop_id, ShopMember.user_id),
                                           ondelete='CASCADE'),
                      Index('shop_user_idx', shop_id, user_id))
