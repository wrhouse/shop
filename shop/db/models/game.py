from sqlalchemy import Column, Integer, String, Text

from shop.migrations import Base


class Game(Base):
    __tablename__ = 'game'

    id = Column(Integer(), primary_key=True)
    name = Column(String(length=64), unique=True, nullable=False)
    description = Column(Text, nullable=False, server_default='')
    url = Column(String(length=255), nullable=False, unique=True)
    image_url = Column(String(length=255), nullable=False, server_default='games/images/default.jpeg')
    chat_image_url = Column(String(length=255), nullable=False, server_default='games/chat_images/default.jpeg')
