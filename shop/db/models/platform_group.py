from sqlalchemy import Column, Integer, String

from shop.migrations import Base


class PlatformGroup(Base):
    __tablename__ = 'platform_group'

    id = Column(Integer(), primary_key=True)
    name = Column(String(length=32), nullable=False)
