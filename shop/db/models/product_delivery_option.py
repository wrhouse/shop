from sqlalchemy import Column, Integer, ForeignKey

from shop.migrations import Base
from .delivery_option import DeliveryOption
from .product import Product


class ProductDeliveryOption(Base):
    __tablename__ = 'product_delivery_option'

    id = Column(Integer(), primary_key=True)
    product_id = Column(Integer(), ForeignKey(Product.id, ondelete='CASCADE'), nullable=False)
    delivery_option_id = Column(Integer(), ForeignKey(DeliveryOption.id, ondelete='CASCADE'), nullable=False)
