from sqlalchemy import Column, Integer, ForeignKey

from shop.migrations import Base
from .delivery_option import DeliveryOption
from .game import Game
from .service import Service


class RelDeliveryOptionGameService(Base):
    __tablename__ = 'rel_delivery_option_game_service'

    delivery_option_id = Column(Integer(), ForeignKey(DeliveryOption.id, ondelete='CASCADE'), nullable=False, primary_key=True)
    game_id = Column(Integer(), ForeignKey(Game.id, ondelete='CASCADE'), nullable=False, primary_key=True)
    service_id = Column(Integer(), ForeignKey(Service.id, ondelete='CASCADE'), nullable=False, primary_key=True)
