from sqlalchemy import Column, Integer, String, Text
from sqlalchemy.sql import literal

from shop.migrations import Base


class Service(Base):
    __tablename__ = 'service'

    id = Column(Integer(), primary_key=True)
    parent_id = Column(Integer(), nullable=False, server_default=literal(0))
    name = Column(String(length=32), nullable=False)
    icon_url = Column(String(length=255), nullable=False, server_default='content/shop/icons/services/default.svg')
    delivery_guidelines = Column(Text(), nullable=False, server_default='')
