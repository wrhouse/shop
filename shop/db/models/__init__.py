from .currency import Currency
from .delivery_option import DeliveryOption
from .game import Game
from .game_option import GameOption
from .game_option_value import GameOptionValue
from .language import Language
from .localization import Localization
from .platform import Platform
from .platform_group import PlatformGroup
from .product import Product
from .product_delivery_option import ProductDeliveryOption
from .product_image import ProductImage
from .product_option_value import ProductOptionValue
from .rel_delivery_option_game_service import RelDeliveryOptionGameService
from .rel_game_platform import RelGamePlatform
from .rel_service_game import RelServiceGame
from .rel_user_shop import RelUserShop
from .service import Service
from .service_duration import ServiceDuration
from .service_insurance import ServiceInsurance
from .shop import Shop
from .shop_member import ShopMember
from .shop_member_permission import ShopMemberPermission


"""
URLs:
    Index:
    - Index [index.html]: /

    Info:
    - Trade protect [for_traders.html]: /info/trade_protect
    - For buyers [for_buyers.html]: /info/for_buyers
    - For sellers [for_sellers.html]: /info/for_sellers
    - About us [about.html]: /info/about_us
    
    Games:
    - All games [games_page.html]: /games
    - Games by platform [games.html]: /games?platform_id={platform_url:str}
    - Trades [type_game.html]: /games/{game_url:str}
    - ??? Product [product.html]: /games/{game_url}/{product_url:str}-{product_id:int}
    
    Store:
    - Store [seller_shop.html]: /store/{store_url:str}
    - Overview [my_gw.html]: /store/{store_url:str}/overview
    - Lots [lots.html]: /store/{store_url:str}/lots
    - ??? Product [product.html]: /store/{store_url:str}/lots/
    
    Order:
    
    
    Personal Area:
    - Profile [pa-profile.html]: /personal
    - My Orders [pa-my-orders.html]: /personal/orders
    - Balance [pa-balance.html]: /personal/balance
    - Deffered [pa-my-deffered.html]: /personal/deffered
    - My Stores [TODO]: /personal/stores


"""
