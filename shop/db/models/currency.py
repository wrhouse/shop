from sqlalchemy import Column, Integer, String

from shop.migrations import Base


class Currency(Base):
    __tablename__ = 'currency'

    id = Column(Integer(), primary_key=True)
    name = Column(String(length=8), nullable=False, unique=True)
    sign = Column(String(length=6), nullable=False)
