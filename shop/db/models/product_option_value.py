from sqlalchemy import Column, Integer, ForeignKey

from shop.migrations import Base
from .game_option_value import GameOptionValue
from .product import Product


class ProductOptionValue(Base):
    __tablename__ = 'product_option_value'

    id = Column(Integer(), primary_key=True)
    product_id = Column(Integer(), ForeignKey(Product.id, ondelete='CASCADE'), nullable=False)
    game_option_value_id = Column(Integer(), ForeignKey(GameOptionValue.id, ondelete='CASCADE'), nullable=False)
