from sqlalchemy import Column, Integer, ForeignKey, DateTime
from sqlalchemy.sql import func

from shop.migrations import Base
from .shop import Shop


class RelUserShop(Base):
    __tablename__ = 'rel_user_shop'

    user_id = Column(Integer(), nullable=False, primary_key=True)
    shop_id = Column(Integer(), ForeignKey(Shop.id, ondelete='CASCADE'), nullable=False, primary_key=True)
    created_at = Column(DateTime(timezone=True), nullable=False, server_default=func.now())

