from sqlalchemy import Column, Integer, ForeignKey, String
from sqlalchemy.sql import literal

from shop.migrations import Base
from .game import Game
from .service import Service


class GameOption(Base):
    __tablename__ = 'game_option'

    id = Column(Integer(), primary_key=True)
    game_id = Column(Integer(), ForeignKey(Game.id, ondelete='CASCADE'), nullable=False)
    service_id = Column(Integer(), ForeignKey(Service.id, ondelete='CASCADE'), nullable=False)
    position = Column(Integer(), nullable=False, server_default=literal(0))
    game_option_name = Column(String(length=32), nullable=False)
