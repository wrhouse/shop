from sqlalchemy import Column, Integer, ForeignKey, String, Text

from shop.migrations import Base
from .shop import Shop


class ShopMember(Base):
    __tablename__ = 'shop_member'

    shop_id = Column(Integer(), ForeignKey(Shop.id, ondelete='CASCADE'), primary_key=True)
    user_id = Column(Integer(), primary_key=True)
    role = Column(String(length=255), nullable=False, server_default='')
    role_description = Column(Text(), nullable=False, server_default='')
