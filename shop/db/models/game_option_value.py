from sqlalchemy import Column, Integer, ForeignKey, String

from shop.migrations import Base
from .game_option import GameOption


class GameOptionValue(Base):
    __tablename__ = 'game_option_value'

    id = Column(Integer(), primary_key=True)
    game_option_id = Column(Integer(), ForeignKey(GameOption.id, ondelete='CASCADE'), nullable=False)
    game_option_value = Column(String(length=32), nullable=False)
