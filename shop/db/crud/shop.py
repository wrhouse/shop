from typing import List, Optional

from aiopg.sa import Engine

from shop.db.models import Shop


async def fn_get_shop(engine: Engine, *,
                      shop_id: Optional[int] = None,
                      owner_id: Optional[int] = None,
                      url: Optional[str] = None,
                      offset: Optional[int] = None,
                      limit: Optional[int] = None) -> List[Shop]:
    select_query = Shop.__table__.select()

    if shop_id is not None:
        select_query.where(Shop.id == shop_id)

    if owner_id is not None:
        select_query.where(Shop.owner_id == owner_id)

    if url is not None:
        select_query.where(Shop.url == url)

    if offset is not None:
        select_query.offset(offset)

    if limit is not None:
        select_query.limit(limit)

    async with engine.acquire() as db_connection:
        cursor = await db_connection.execute(select_query)
        shops = await cursor.fetchall()

    return shops
