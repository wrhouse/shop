import asyncio
from collections import namedtuple
from typing import Optional, List, Union, Tuple

from aiopg.sa import Engine

from shop.db.models import Product
from shop.utils.images import create_and_upload_image, ResizeMode
from .product_image import (
    fn_create_product_image,
    fn_get_product_image
)
from .product_delivery_option import (
    fn_create_product_delivery_option,
    fn_get_product_delivery_option
)
from .product_option_value import (
    fn_create_product_option_value,
    fn_get_product_option_value
)


ExtendedProduct = namedtuple('ExtendedProduct', [
    'product',
    'product_delivery_options',
    'product_images',
    'product_option_values'
])


async def fn_get_product(engine: Engine, *,
                         product_ids: Optional[List[int]] = None,
                         shop_id: Optional[int] = None,
                         game_id: Optional[int] = None,
                         service_ids: Optional[List[int]] = None,
                         platform_id: Optional[int] = None,
                         limit: Optional[int] = None,
                         offset: Optional[int] = None,
                         extended: bool = False) -> List[Union[Product, ExtendedProduct]]:
    select_query = Product.__table__.select()

    if product_ids is not None:
        if len(product_ids) == 1:
            select_query = select_query.where(Product.id == product_ids[0])
        else:
            select_query = select_query.where(Product.id.in_(product_ids))

    if shop_id is not None:
        select_query = select_query.where(Product.shop_id == shop_id)

    if game_id is not None:
        select_query = select_query.where(Product.game_id == game_id)

    if service_ids:
        if len(service_ids) == 1:
            select_query = select_query.where(Product.service_id == service_ids[0])
        else:
            select_query = select_query.where(Product.service_id.in_(service_ids))

    if platform_id is not None:
        select_query = select_query.where(Product.platform_id == platform_id)

    if limit is not None:
        select_query = select_query.limit(limit)

    if offset is not None:
        select_query = select_query.offset(offset)

    async with engine.acquire() as db_connection:
        cursor = await db_connection.execute(select_query)
        products = await cursor.fetchall()

    if extended:
        products = [ExtendedProduct(product=product,
                                    product_delivery_options=await fn_get_product_delivery_option(engine,
                                                                                                  product_id=product.id),
                                    product_images=await fn_get_product_image(engine,
                                                                              product_id=product.id),
                                    product_option_values=await fn_get_product_option_value(engine,
                                                                                            product_id=product.id))
                    for product in products]

    return products


async def fn_create_product(engine: Engine,
                            *,
                            shop_id: int,
                            author_id: int,
                            game_id: int,
                            service_id: int,
                            platform_id: int,
                            game_option_value_ids: List[int],
                            name: str,
                            url: str,
                            description: str,
                            images: List[str],
                            image_size: Tuple[int, int],
                            thumbnail_size: Tuple[int, int],
                            currency_id: int,
                            price_per_unit: int,
                            stock: int,
                            min_unit_per_order: int,
                            insurance_id: Optional[int],
                            duration_id: Optional[int],
                            delivery_option_ids: List[int],
                            online_hrs: int,
                            offline_hrs: int) -> Optional[int]:
    upload_image_tasks = [create_and_upload_image(image,
                                                  size=image_size,
                                                  resize_mode=ResizeMode.THUMBNAIL_MODE)
                          for image in images]
    if images:
        upload_image_tasks.append(create_and_upload_image(images[0],
                                                          size=thumbnail_size,
                                                          resize_mode=ResizeMode.THUMBNAIL_MODE))

    image_urls = None
    thumbnail_url = None
    if upload_image_tasks:
        image_urls = await asyncio.gather(*upload_image_tasks)
        thumbnail_url = image_urls[-1]

    async with engine.acquire() as connection:
        product_insert_query = (Product.__table__.insert()
                                .values(shop_id=shop_id,
                                        name=name,
                                        url=url,
                                        description=description,
                                        thumbnail_url=thumbnail_url,
                                        currency_id=currency_id,
                                        price_per_unit=price_per_unit,
                                        stock=stock,
                                        min_unit_per_order=min_unit_per_order,
                                        game_id=game_id,
                                        service_id=service_id,
                                        platform_id=platform_id,
                                        insurance_id=insurance_id,
                                        duration_id=duration_id,
                                        online_hrs=online_hrs,
                                        offline_hrs=offline_hrs,
                                        author_id=author_id))
        product_id = await connection.scalar(product_insert_query)

        if image_urls is not None:
            for i in range(len(image_urls) - 1):
                await fn_create_product_image(connection,
                                              product_id=product_id,
                                              image_url=image_urls[i])

        for game_option_value_id in game_option_value_ids:
            await fn_create_product_option_value(connection,
                                                 product_id=product_id,
                                                 game_option_value_id=game_option_value_id)

        for delivery_option_id in delivery_option_ids:
            await fn_create_product_delivery_option(connection,
                                                    product_id=product_id,
                                                    delivery_option_id=delivery_option_id)

        return product_id
