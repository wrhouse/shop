from typing import Optional, List

from aiopg.sa import Engine, SAConnection

from shop.db.models import ProductImage


async def fn_get_product_image(engine: Engine,
                               *,
                               product_image_id: Optional[int] = None,
                               product_id: Optional[int] = None,
                               url: Optional[str] = None) -> List[ProductImage]:
    select_query = ProductImage.__table__.select()

    if product_image_id is not None:
        select_query = select_query.where(ProductImage.id == product_image_id)

    if product_id is not None:
        select_query = select_query.where(ProductImage.product_id == product_id)

    if url is not None:
        select_query = select_query.where(ProductImage.url == url)

    async with engine.acquire() as connection:
        cursor = await connection.execute(select_query)
        product_images = await cursor.fetchall()

    return product_images


async def fn_create_product_image(connection: SAConnection,
                                  *,
                                  product_id: int,
                                  image_url: str) -> int:
    insert_query = (ProductImage.__table__.insert().values(product_id=product_id,
                                                           url=image_url))
    return await connection.scalar(insert_query)
