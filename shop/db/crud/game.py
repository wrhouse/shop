from collections import namedtuple
from typing import List, Optional, Union

from aiopg.sa import Engine
from sqlalchemy.orm.attributes import InstrumentedAttribute

from shop.db.models import Game, RelGamePlatform
from shop.db.models import RelServiceGame
from .platform import fn_get_platform
from .service import fn_get_service

ExtendedGame = namedtuple('ExtendedGame', [
    'game',
    'platforms',
    'services'
])


async def fn_get_game(engine: Engine,
                      *,
                      game_ids: Optional[List[int]] = None,
                      platforms: Optional[List[int]] = None,
                      services: Optional[List[int]] = None,
                      limit: Optional[int] = None,
                      offset: Optional[int] = None,
                      order_field: Optional[InstrumentedAttribute] = None,
                      extended: Optional[bool] = False) -> List[Union[Game, ExtendedGame]]:
    game_query = Game.__table__.select()

    if game_ids:
        if len(game_ids) == 1:
            game_query = game_query.where(Game.id == game_ids[0])
        else:
            game_query = game_query.where(Game.id.in_(game_ids))

    if services:
        service_query = RelServiceGame.__table__.select()
        if len(services) == 1:
            service_query = service_query.where(RelServiceGame.service_id == services[0])
        else:
            service_query = service_query.where(RelServiceGame.service_id.in_(services))
        service_query = service_query.with_only_columns([RelServiceGame.game_id])
        game_query = game_query.where(Game.id.in_(service_query))

    if platforms:
        product_query = RelGamePlatform.__table__.select()
        if len(platforms) == 1:
            product_query = product_query.where(RelGamePlatform.platform_id == platforms[0])
        else:
            product_query = product_query.where(RelGamePlatform.platform_id.in_(platforms))
        product_query = product_query.with_only_columns([RelGamePlatform.game_id])
        game_query = game_query.where(Game.id.in_(product_query))

    if limit:
        game_query = game_query.limit(limit)

    if offset:
        game_query = game_query.offset(offset)

    if order_field:
        game_query = game_query.order_by(order_field)

    async with engine.acquire() as connection:
        cursor = await connection.execute(game_query)
        games = await cursor.fetchall()

    if extended:
        games = [ExtendedGame(game=game,
                              platforms=await fn_get_platform(engine, game_id=game.id),
                              services=await fn_get_service(engine, game_id=game.id))
                 for game in games]

    return games
