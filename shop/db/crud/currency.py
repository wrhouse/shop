from typing import List, Optional

from aiopg.sa import Engine

from shop.db.models import Currency


async def fn_get_currency(engine: Engine,
                          *,
                          currency_id: Optional[int] = None,
                          currency_name: Optional[str] = None) -> List[Currency]:
    select_query = Currency.__table__.select()
    if currency_id is not None:
        select_query = select_query.where(Currency.id == currency_id)
    if currency_name is not None:
        select_query = select_query.where(Currency.name == currency_name)

    async with engine.acquire() as connection:
        cursor = await connection.execute(select_query)
        return await cursor.fetchall()
