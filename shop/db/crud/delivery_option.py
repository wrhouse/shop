from typing import List, Optional

from aiopg.sa import Engine
from sqlalchemy import join, and_

from shop.db.models import DeliveryOption, RelDeliveryOptionGameService


async def fn_get_delivery_option(engine: Engine,
                                 *,
                                 game_id: Optional[int] = None,
                                 service_id: Optional[int] = None) -> List[DeliveryOption]:
    select_query = DeliveryOption.__table__.select()
    if game_id is not None and service_id is not None:
        join_subquery = join(DeliveryOption,
                             RelDeliveryOptionGameService,
                             DeliveryOption.id == RelDeliveryOptionGameService.delivery_option_id)
        select_query = (select_query
                        .select_from(join_subquery)
                        .where(and_(RelDeliveryOptionGameService.service_id == service_id,
                                    RelDeliveryOptionGameService.game_id == game_id)))

    async with engine.acquire() as connection:
        cursor = await connection.execute(select_query)
        return await cursor.fetchall()
