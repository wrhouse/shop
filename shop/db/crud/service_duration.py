from typing import Optional, List

from aiopg.sa import Engine

from shop.db.models import ServiceDuration


async def fn_get_service_duration(engine: Engine,
                                  *,
                                  service_id: Optional[int]) -> List[ServiceDuration]:
    select_query = ServiceDuration.__table__.select()
    if service_id is not None:
        select_query = select_query.where(ServiceDuration.service_id == service_id)

    async with engine.acquire() as connection:
        cursor = await connection.execute(select_query)
        return await cursor.fetchall()
