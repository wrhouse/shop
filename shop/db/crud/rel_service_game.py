from typing import List, Optional

from aiopg.sa import Engine

from shop.db.models import RelServiceGame


async def fn_get_rel_service_game(engine: Engine,
                                  *,
                                  game_id: Optional[int] = None,
                                  service_id: Optional[int] = None) -> List[RelServiceGame]:
    select_query = RelServiceGame.__table__.select()
    if game_id is not None:
        select_query = select_query.where(RelServiceGame.game_id == game_id)
    if service_id is not None:
        select_query = select_query.where(RelServiceGame.service_id == service_id)
    async with engine.acquire() as connection:
        cursor = await connection.execute(select_query)
        return await cursor.fetchall()
