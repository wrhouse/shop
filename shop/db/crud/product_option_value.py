from typing import List, Optional

from aiopg.sa import Engine, SAConnection

from shop.db.models import ProductOptionValue


async def fn_create_product_option_value(connection: SAConnection,
                                         *,
                                         product_id: int,
                                         game_option_value_id: int) -> int:
    insert_query = (ProductOptionValue.__table__.insert().values(product_id=product_id,
                                                                 game_option_value_id=game_option_value_id))
    return await connection.scalar(insert_query)


async def fn_get_product_option_value(engine: Engine, *,
                                      product_option_value_id: Optional[int] = None,
                                      product_id: Optional[int] = None,
                                      game_option_value_ids: Optional[List[int]] = None):
    select_query = ProductOptionValue.__table__.select()

    if product_option_value_id is not None:
        select_query = select_query.where(ProductOptionValue.id == product_option_value_id)

    if product_id is not None:
        select_query = select_query.where(ProductOptionValue.product_id == product_id)

    if game_option_value_ids is not None:
        select_query = select_query.where(ProductOptionValue.game_option_value_id.in_(game_option_value_ids))

    async with engine.acquire() as db_connection:
        cursor = await db_connection.execute(select_query)
        product_option_values = await cursor.fetchall()

    return product_option_values
