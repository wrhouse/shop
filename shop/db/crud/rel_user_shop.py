from typing import Optional, List, Tuple

from aiopg.sa import Engine
from sqlalchemy.sql import select, func

from shop.db.models import RelUserShop


async def fn_count_rel_user_shop(engine: Engine,
                                 *,
                                 user_id: Optional[int] = None,
                                 shop_id: Optional[int] = None) -> int:
    select_count_query = select([func.count()]).select_from(RelUserShop)
    if user_id is not None:
        select_count_query = select_count_query.where(RelUserShop.user_id == user_id)
    if shop_id is not None:
        select_count_query = select_count_query.where(RelUserShop.shop_id == shop_id)

    async with engine.acquire() as connection:
        return await connection.scalar(select_count_query)


async def fn_get_rel_user_shop(engine: Engine,
                               *,
                               user_id: Optional[int] = None,
                               shop_id: Optional[int] = None) -> List[RelUserShop]:
    select_query = RelUserShop.__table__.select()
    if user_id is not None:
        select_query = select_query.where(RelUserShop.user_id == user_id)
    if shop_id is not None:
        select_query = select_query.where(RelUserShop.shop_id == shop_id)

    async with engine.acquire() as connection:
        cursor = await connection.execute(select_query)
        return await cursor.fetchall()


async def fn_create_rel_user_shop(engine: Engine,
                                  *,
                                  user_id: int,
                                  shop_id: int) -> Tuple[int, int]:
    is_exists = bool(await fn_count_rel_user_shop(engine, user_id=user_id, shop_id=shop_id))
    if not is_exists:
        insert_query = (RelUserShop.__table__
                        .insert()
                        .values(user_id=user_id,
                                shop_id=shop_id))

        async with engine.acquire() as connection:
            await connection.execute(insert_query)

    return user_id, shop_id


async def fn_delete_rel_user_shop(engine: Engine,
                                  *,
                                  user_id: Optional[int] = None,
                                  shop_id: Optional[int] = None) -> bool:
    delete_query = RelUserShop.__table__.delete()
    if user_id is not None:
        delete_query = delete_query.where(RelUserShop.user_id == user_id)
    if shop_id is not None:
        delete_query = delete_query.where(RelUserShop.shop_id == shop_id)

    async with engine.acquire() as connection:
        await connection.execute(delete_query)

    return True
