from typing import Optional, List

from aiopg.sa import Engine

from shop.db.models import ServiceInsurance


async def fn_get_service_insurance(engine: Engine,
                                   *,
                                   service_id: Optional[int]) -> List[ServiceInsurance]:
    select_query = ServiceInsurance.__table__.select()
    if service_id is not None:
        select_query = select_query.where(ServiceInsurance.service_id == service_id)

    async with engine.acquire() as connection:
        cursor = await connection.execute(select_query)
        return await cursor.fetchall()
