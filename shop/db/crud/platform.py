from typing import List, Optional

from aiopg.sa import Engine
from sqlalchemy import join

from shop.db.models import Platform, PlatformGroup, RelGamePlatform


async def fn_get_platform(engine: Engine,
                          *,
                          platform_id: Optional[int] = None,
                          game_id: Optional[int] = None,
                          platform_group_id: Optional[int] = None,
                          platform_group_name: Optional[int] = None) -> List[Platform]:
    select_query = Platform.__table__.select()

    if platform_group_id is not None or platform_group_name is not None:
        join_subquery = join(Platform,
                             PlatformGroup,
                             Platform.group_id == PlatformGroup.id)
        select_query = select_query.select_from(join_subquery)
        if platform_group_id is not None:
            select_query = select_query.where(PlatformGroup.id == platform_group_id)
        if platform_group_name is not None:
            select_query = select_query.where(PlatformGroup.name == platform_group_name)

    if game_id is not None:
        join_subquery = join(Platform,
                             RelGamePlatform,
                             Platform.id == RelGamePlatform.platform_id)
        select_query = (select_query
                        .select_from(join_subquery)
                        .where(RelGamePlatform.game_id == game_id))

    if platform_id is not None:
        select_query = select_query.where(Platform.id == platform_id)

    async with engine.acquire() as connection:
        cursor = await connection.execute(select_query)
        platforms = await cursor.fetchall()

    return platforms
