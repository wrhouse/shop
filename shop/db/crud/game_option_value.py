from typing import List, Optional

from aiopg.sa import Engine

from shop.db.models import GameOptionValue


async def fn_get_game_option_value(engine: Engine,
                                   *,
                                   game_option_value_id: Optional[int] = None,
                                   game_option_id: Optional[int] = None) -> List[GameOptionValue]:
    select_query = GameOptionValue.__table__.select()
    if game_option_value_id is not None:
        select_query = select_query.where(GameOptionValue.id == game_option_value_id)
    if game_option_id is not None:
        select_query = select_query.where(GameOptionValue.game_option_id == game_option_id)

    async with engine.acquire() as connection:
        cursor = await connection.execute(select_query)
        game_option_values = await cursor.fetchall()

    return game_option_values
