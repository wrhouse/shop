from typing import Optional, List

from aiopg.sa import Engine, SAConnection

from shop.db.models import ProductDeliveryOption


async def fn_get_product_delivery_option(engine: Engine,
                                         *,
                                         product_delivery_option_id: Optional[int] = None,
                                         product_id: Optional[int] = None,
                                         delivery_option_id: Optional[int] = None) -> List[ProductDeliveryOption]:
    select_query = ProductDeliveryOption.__table__.select()

    if product_delivery_option_id is not None:
        select_query = select_query.where(ProductDeliveryOption.id == product_delivery_option_id)

    if product_id is not None:
        select_query = select_query.where(ProductDeliveryOption.product_id == product_id)

    if delivery_option_id is not None:
        select_query = select_query.where(ProductDeliveryOption.delivery_option_id == delivery_option_id)

    async with engine.acquire() as connection:
        cursor = await connection.execute(select_query)
        product_delivery_options = await cursor.fetchall()

    return product_delivery_options


async def fn_create_product_delivery_option(connection: SAConnection,
                                            *,
                                            product_id: int,
                                            delivery_option_id: int) -> int:
    insert_query = (ProductDeliveryOption.__table__.insert().values(product_id=product_id,
                                                                    delivery_option_id=delivery_option_id))
    return await connection.scalar(insert_query)
