from collections import namedtuple
from typing import List, Optional, Union

from aiopg.sa import Engine
from sqlalchemy import join, select

from shop.db.models import Service, RelServiceGame
from .service_duration import fn_get_service_duration
from .service_insurance import fn_get_service_insurance

ExtendedService = namedtuple('ExtendedService', [
    'service',
    'duration_days',
    'insurance_days'
])


async def fn_get_service(engine: Engine,
                         *,
                         service_id: Optional[int] = None,
                         parent_id: Optional[int] = None,
                         game_id: Optional[int] = None,
                         extended: bool = False) -> List[Union[Service, ExtendedService]]:
    select_query = Service.__table__.select()

    if game_id is not None:
        join_subquery = join(Service,
                             RelServiceGame,
                             Service.id == RelServiceGame.service_id)
        select_query = (select_query
                        .select_from(join_subquery)
                        .where(RelServiceGame.game_id == game_id))

    if service_id is not None:
        select_query = select_query.where(Service.id == service_id)

    if parent_id is not None:
        select_query = select_query.where(Service.parent_id == parent_id)

    async with engine.acquire() as connection:
        cursor = await connection.execute(select_query)
        services = await cursor.fetchall()

    if extended:
        services = [ExtendedService(service=service,
                                    insurance_days=await fn_get_service_insurance(engine,
                                                                                  service_id=service.id),
                                    duration_days=await fn_get_service_duration(engine,
                                                                                service_id=service.id))
                    for service in services]

    return services


async def fn_get_service_hierarchy(engine: Engine,
                                   *,
                                   service_id: int) -> List[Service]:
    service = (await fn_get_service(engine, service_id=service_id))[0]
    services = [service]

    while service.parent_id != 0:
        service = (await fn_get_service(engine, service_id=service.parent_id))[0]
        services.append(service)

    return list(reversed(services))


async def fn_get_service_children(engine: Engine,
                                  *,
                                  service_id: int) -> List[int]:
    select_query = select([Service.id, Service.parent_id])

    async with engine.acquire() as db_connection:
        cursor = await db_connection.execute(select_query)
        all_services = await cursor.fetchall()

    children = [service_id]
    found_services = [service.id for service in all_services if service.parent_id == service_id]

    while found_services:
        children.extend(found_services)
        found_services = [service.id for service in all_services if service.parent_id in found_services]

    return children
