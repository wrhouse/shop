from collections import namedtuple
from typing import List, Optional, Union

from aiopg.sa import Engine

from shop.db.models import GameOption
from .game_option_value import fn_get_game_option_value


ExtendedGameOption = namedtuple('ExtendedGameOption', [
    'option',
    'values'
])


async def fn_get_game_option(engine: Engine,
                             *,
                             game_option_id: Optional[int] = None,
                             game_id: Optional[int] = None,
                             service_id: Optional[int] = None,
                             extended: bool = False) -> List[Union[GameOption, ExtendedGameOption]]:
    select_query = GameOption.__table__.select()

    if game_option_id is not None:
        select_query = select_query.where(GameOption.id == game_option_id)

    if game_id is not None:
        select_query = select_query.where(GameOption.game_id == game_id)

    if service_id is not None:
        select_query = select_query.where(GameOption.service_id == service_id)

    if game_id is not None and service_id is not None:
        select_query = select_query.order_by(GameOption.position.asc())

    async with engine.acquire() as connection:
        cursor = await connection.execute(select_query)
        game_options = await cursor.fetchall()

    if extended:
        game_options = [ExtendedGameOption(option=game_option,
                                           values=await fn_get_game_option_value(engine,
                                                                                 game_option_id=game_option.id))
                        for game_option in game_options]

    return game_options
