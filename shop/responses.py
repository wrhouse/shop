from typing import List, Optional

from aiohttp.web import HTTPTemporaryRedirect
from aiohttp.web_exceptions import HTTPException
import ujson as json

from shop.settings import Settings


settings = Settings()


class ServiceRedirect(HTTPTemporaryRedirect):
    def __init__(self, service: str, path: str):
        location = f"https://{service}.{settings.service_domain}/{path.lstrip('/')}"
        super().__init__(location=location)


class ServiceException(HTTPException):
    status_code = 400

    def __init__(self,
                 error_code: int,
                 error_message: str,
                 content_type: str = "application/json"):
        response_data = {
            "success": False,
            "error": {"code": error_code, "message": error_message},
        }
        super().__init__(text=json.dumps(response_data), content_type=content_type)

        self.error_code = error_code
        self.error_message = error_message


class InternalServerError(ServiceException):
    status_code = 500

    def __init__(self):
        super().__init__(error_code=1000, error_message="Internal server error")


class MissingParameter(ServiceException):
    def __init__(self, param_name: str):
        super().__init__(error_code=1001, error_message=f"Missing parameter: {param_name}")


class InvalidParameter(ServiceException):
    def __init__(self, param_name: str):
        super().__init__(error_code=1002, error_message=f"Invalid parameter: {param_name}")


class DuplicateError(ServiceException):
    def __init__(self, param_name: str):
        super().__init__(error_code=1003, error_message=f"Duplicate parameter: {param_name}")


class ValidationParametersError(ServiceException):
    def __init__(self, messages: List[str]):
        super().__init__(error_code=1004, error_message='; '.join(messages))


class AccessTokenExpired(ServiceException):
    def __init__(self):
        super().__init__(error_code=1005, error_message='Access token has been expired')


class AccessTokenDeleted(ServiceException):
    def __init__(self):
        super().__init__(error_code=1006, error_message='Access token has been deleted')


class RefreshTokenExpired(ServiceException):
    def __init__(self):
        super().__init__(error_code=1007, error_message='Refresh token has been expired')


class AccessDeniedError(ServiceException):
    def __init__(self, permissions: Optional[List[str]] = None):
        error_message = 'Access denied'
        if permissions:
            error_message = 'No permissions: ' + ', '.join(permissions)
        super().__init__(error_code=1008, error_message=error_message)
