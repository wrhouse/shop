import re
from typing import Any, Callable, List, Union, Tuple

from unidecode import unidecode


__WHITESPACE_PATTERN__ = re.compile('\s+')
__ANOTHER_SYMBOLS__ = re.compile('[^\s\w\d]+')


def url_encode(text: str) -> str:
    translited_text = unidecode(text).lower().strip()
    text_without_another_symbols = __ANOTHER_SYMBOLS__.sub('', translited_text)
    text_without_whitespaces = __WHITESPACE_PATTERN__.sub('_', text_without_another_symbols)
    return text_without_whitespaces


def json_formatter(fields: List[Union[Tuple[Any, str], Any]]) -> Callable[[Any], dict]:
    def formatter(obj: Any) -> dict:
        result = dict()
        for field in fields:
            field_name, alias = field, field
            if type(field) == tuple:
                field_name, alias = field
            result[alias] = obj[field_name]

        return result
    return formatter
