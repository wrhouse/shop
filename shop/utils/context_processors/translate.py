from typing import Callable, List, Optional
from itertools import chain

import aiohttp.web


DEFAULT_LANG_CODE = 'en'


def parse_accept_language(accept_language: Optional[str]):
    if accept_language is None:
        return

    languages = accept_language.split(",")
    for language in languages:
        if language.split(";")[0] == language:
            yield language.strip()
        else:
            yield language.split(";")[0].strip()


def translate_func(app: aiohttp.web.Application,
                   lang_codes: Optional[List[str]] = None,
                   accept_language: Optional[str] = None) -> Callable:
    if lang_codes is None:
        lang_codes = []

    default_dict = app['locales'].get(DEFAULT_LANG_CODE, {})

    translate_dict = {}
    lang_code_iterator = chain(lang_codes, parse_accept_language(accept_language))
    for lang_code in lang_code_iterator:
        if lang_code in app['locales']:
            translate_dict = app['locales'][lang_code]
            break

    def func(text_code: str) -> str:
        return translate_dict.get(text_code, default_dict.get(text_code, text_code))

    return func


def translate_func_ctx(request: aiohttp.web.Request) -> Callable:
    lang_codes = []
    if 'lang_code' in request.cookies:
        lang_codes.append(request.cookies['lang_code'])

    return translate_func(app=request.app,
                          lang_codes=lang_codes,
                          accept_language=request.headers.get('Accept-Language'))


__all__ = ['translate_func', 'translate_func_ctx']
