from datetime import datetime, timedelta

from jwcrypto import jwk, jwt
import ujson as json


def decode_payload(token: str,
                   secret_key: str) -> dict:
    key = jwk.JWK(k=secret_key, kty='oct')
    decoded_token = jwt.JWT(key=key, jwt=token)
    payload = json.loads(decoded_token.claims)
    if 'created_at' in payload:
        payload['created_at'] = datetime.fromisoformat(payload['created_at'])
    return payload


def token_is_expired(payload: dict,
                     expire_in: int) -> bool:
    if 'created_at' not in payload:
        return False
    current_time = datetime.utcnow()
    return (payload['created_at'] + timedelta(seconds=expire_in)) < current_time
