from aiohttp import web
import ujson

def success_response(data):
    """
    Returns error message with following format:
    >>> success_response("user was updated")
    {
        "success": true,
        "data": "user was updated"
    }
    :param data:
    :return:
    """
    response = {
        "success": True,
        "data": data
    }
    return web.json_response(response, dumps=ujson.dumps)


def error_response(message, http_code=400, error_code=None) -> web.Response:
    """
    Returns error message with following format:
    >>> error_response("error message", 200, 400)
    {
        "success": false,
        "error": {
            "code": 400,
            "message": "error message"
        }
    }
    :param message: error description
    :param http_code: http code of returning response
    :param error_code: error code, described in confluence.
    :return: web.Response
    """
    if error_code is None:
        error_code = http_code
    response = {
        "success": False,
        "error": {
            "code": error_code,
            "message": message
        }
    }
    return web.json_response(response, status=http_code, dumps=ujson.dumps)
