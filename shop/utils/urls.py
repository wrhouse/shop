import os


def include_urls(app, prefix, module) -> None:
    module.init_routes(app, prefix)


def include_static_path(app, base_path: str, static_path: str) -> None:
    app.router.add_static(prefix=static_path,
                          path=os.path.join(base_path, static_path.strip('/')),
                          name=static_path.strip('/').replace('/', '.'))
