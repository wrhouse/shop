FROM python:3.7-alpine

MAINTAINER v.bakaev <vlad@bakaev.tech>

WORKDIR /app

RUN apk add --no-cache g++ \
                       gcc \
                       bash \
                       make \
                       postgresql-dev \
                       linux-headers \
                       libgcc \
                       musl-dev \
                       jpeg-dev \
                       zlib-dev \
                       git \
                       openssh-client \
                       libressl-dev \
                       libffi-dev

RUN mkdir -p /root/.ssh && \
    chmod 0700 /root/.ssh && \
    ssh-keyscan gitlab.com >> /root/.ssh/known_hosts

ADD ./id_rsa /root/.ssh/
ADD ./id_rsa.pub /root/.ssh/

RUN chmod 600 /root/.ssh/id_rsa && \
    chmod 600 /root/.ssh/id_rsa.pub && \
    ssh -T git@gitlab.com

ARG requirements=requirements/production.txt
COPY requirements requirements

RUN pip install --no-cache-dir cython
RUN pip install --no-cache-dir -r $requirements

ADD . /app
RUN pip install --no-cache-dir -e .

RUN rm -rf /root/.ssh/

