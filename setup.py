import os
import re
import pathlib
from typing import List

from setuptools import find_packages, setup

REGEXP = re.compile(r"^__version__\W*=\W*'([\d.abrc]+)'")
PARENT = pathlib.Path(__file__).parent


def read_version():
    init_py = os.path.join(
        os.path.dirname(__file__),
        'shop',
        '__init__.py',
    )

    with open(init_py) as f:
        for line in f:
            match = REGEXP.match(line)
            if match is not None:
                return match.group(1)
        else:
            msg = f'Cannot find version in ${init_py}'
            raise RuntimeError(msg)


def read_requirements(path: str, links: bool = False) -> List[str]:
    file_path = PARENT / path
    with open(file_path) as f:
        contents = f.read().split('\n')
    if links:
        filter_func = lambda x: x.startswith('git+')
    else:
        filter_func = lambda x: not x.startswith('git+')
    return list(filter(filter_func, contents))


setup(
    name='shop',
    version=read_version(),
    description='Shop Service',
    platforms=['POSIX'],
    packages=find_packages(),
    install_requires=read_requirements('requirements/production.txt'),
    dependency_links=read_requirements('requirements/production.txt', links=True),
    zip_safe=False,
)

