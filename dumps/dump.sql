--
-- PostgreSQL database dump
--

-- Dumped from database version 11.5
-- Dumped by pg_dump version 11.5

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: alembic_version; Type: TABLE; Schema: public; Owner: shop_admin
--

CREATE TABLE public.alembic_version (
    version_num character varying(32) NOT NULL
);


ALTER TABLE public.alembic_version OWNER TO shop_admin;

--
-- Name: currency; Type: TABLE; Schema: public; Owner: shop_admin
--

CREATE TABLE public.currency (
    id integer NOT NULL,
    name character varying(8) NOT NULL,
    sign character varying(6) NOT NULL
);


ALTER TABLE public.currency OWNER TO shop_admin;

--
-- Name: currency_id_seq; Type: SEQUENCE; Schema: public; Owner: shop_admin
--

CREATE SEQUENCE public.currency_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.currency_id_seq OWNER TO shop_admin;

--
-- Name: currency_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: shop_admin
--

ALTER SEQUENCE public.currency_id_seq OWNED BY public.currency.id;


--
-- Name: delivery_option; Type: TABLE; Schema: public; Owner: shop_admin
--

CREATE TABLE public.delivery_option (
    id integer NOT NULL,
    name character varying(32) NOT NULL
);


ALTER TABLE public.delivery_option OWNER TO shop_admin;

--
-- Name: delivery_option_id_seq; Type: SEQUENCE; Schema: public; Owner: shop_admin
--

CREATE SEQUENCE public.delivery_option_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.delivery_option_id_seq OWNER TO shop_admin;

--
-- Name: delivery_option_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: shop_admin
--

ALTER SEQUENCE public.delivery_option_id_seq OWNED BY public.delivery_option.id;


--
-- Name: game; Type: TABLE; Schema: public; Owner: shop_admin
--

CREATE TABLE public.game (
    id integer NOT NULL,
    name character varying(64) NOT NULL,
    description text DEFAULT ''::text NOT NULL,
    url character varying(255) NOT NULL,
    image_url character varying(255) DEFAULT 'games/images/default.jpeg'::character varying NOT NULL,
    chat_image_url character varying(255) DEFAULT 'games/chat_images/default.jpeg'::character varying NOT NULL
);


ALTER TABLE public.game OWNER TO shop_admin;

--
-- Name: game_id_seq; Type: SEQUENCE; Schema: public; Owner: shop_admin
--

CREATE SEQUENCE public.game_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.game_id_seq OWNER TO shop_admin;

--
-- Name: game_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: shop_admin
--

ALTER SEQUENCE public.game_id_seq OWNED BY public.game.id;


--
-- Name: game_option; Type: TABLE; Schema: public; Owner: shop_admin
--

CREATE TABLE public.game_option (
    id integer NOT NULL,
    game_id integer NOT NULL,
    service_id integer NOT NULL,
    game_option_name character varying(32) NOT NULL
);


ALTER TABLE public.game_option OWNER TO shop_admin;

--
-- Name: game_option_id_seq; Type: SEQUENCE; Schema: public; Owner: shop_admin
--

CREATE SEQUENCE public.game_option_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.game_option_id_seq OWNER TO shop_admin;

--
-- Name: game_option_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: shop_admin
--

ALTER SEQUENCE public.game_option_id_seq OWNED BY public.game_option.id;


--
-- Name: game_option_value; Type: TABLE; Schema: public; Owner: shop_admin
--

CREATE TABLE public.game_option_value (
    id integer NOT NULL,
    game_option_id integer NOT NULL,
    game_option_value character varying(32) NOT NULL
);


ALTER TABLE public.game_option_value OWNER TO shop_admin;

--
-- Name: game_option_value_id_seq; Type: SEQUENCE; Schema: public; Owner: shop_admin
--

CREATE SEQUENCE public.game_option_value_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.game_option_value_id_seq OWNER TO shop_admin;

--
-- Name: game_option_value_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: shop_admin
--

ALTER SEQUENCE public.game_option_value_id_seq OWNED BY public.game_option_value.id;


--
-- Name: language; Type: TABLE; Schema: public; Owner: shop_admin
--

CREATE TABLE public.language (
    lang_code character varying(2) NOT NULL,
    lang_name character varying(32) NOT NULL
);


ALTER TABLE public.language OWNER TO shop_admin;

--
-- Name: localization; Type: TABLE; Schema: public; Owner: shop_admin
--

CREATE TABLE public.localization (
    text_code character varying(255) NOT NULL,
    lang_code character varying(2) NOT NULL,
    text_value text NOT NULL
);


ALTER TABLE public.localization OWNER TO shop_admin;

--
-- Name: platform; Type: TABLE; Schema: public; Owner: shop_admin
--

CREATE TABLE public.platform (
    id integer NOT NULL,
    group_id integer NOT NULL,
    name character varying(32) NOT NULL
);


ALTER TABLE public.platform OWNER TO shop_admin;

--
-- Name: platform_group; Type: TABLE; Schema: public; Owner: shop_admin
--

CREATE TABLE public.platform_group (
    id integer NOT NULL,
    name character varying(32) NOT NULL
);


ALTER TABLE public.platform_group OWNER TO shop_admin;

--
-- Name: platform_group_id_seq; Type: SEQUENCE; Schema: public; Owner: shop_admin
--

CREATE SEQUENCE public.platform_group_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.platform_group_id_seq OWNER TO shop_admin;

--
-- Name: platform_group_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: shop_admin
--

ALTER SEQUENCE public.platform_group_id_seq OWNED BY public.platform_group.id;


--
-- Name: platform_id_seq; Type: SEQUENCE; Schema: public; Owner: shop_admin
--

CREATE SEQUENCE public.platform_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.platform_id_seq OWNER TO shop_admin;

--
-- Name: platform_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: shop_admin
--

ALTER SEQUENCE public.platform_id_seq OWNED BY public.platform.id;


--
-- Name: product; Type: TABLE; Schema: public; Owner: shop_admin
--

CREATE TABLE public.product (
    id integer NOT NULL,
    shop_id integer NOT NULL,
    name character varying(64) NOT NULL,
    url character varying(255) NOT NULL,
    description text DEFAULT ''::text NOT NULL,
    thumbnail_url character varying(255) DEFAULT 'products/thumbnails/default.jpeg'::character varying NOT NULL,
    currency_id integer NOT NULL,
    price_per_unit integer NOT NULL,
    stock integer DEFAULT 1 NOT NULL,
    min_unit_per_order integer NOT NULL,
    game_id integer NOT NULL,
    service_id integer NOT NULL,
    platform_id integer NOT NULL,
    insurance_id integer,
    duration_id integer NOT NULL,
    online_hrs integer NOT NULL,
    offline_hrs integer NOT NULL,
    author_id integer NOT NULL,
    created_at timestamp without time zone DEFAULT now() NOT NULL
);


ALTER TABLE public.product OWNER TO shop_admin;

--
-- Name: product_delivery_option; Type: TABLE; Schema: public; Owner: shop_admin
--

CREATE TABLE public.product_delivery_option (
    id integer NOT NULL,
    product_id integer NOT NULL,
    delivery_option_id integer NOT NULL
);


ALTER TABLE public.product_delivery_option OWNER TO shop_admin;

--
-- Name: product_delivery_option_id_seq; Type: SEQUENCE; Schema: public; Owner: shop_admin
--

CREATE SEQUENCE public.product_delivery_option_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.product_delivery_option_id_seq OWNER TO shop_admin;

--
-- Name: product_delivery_option_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: shop_admin
--

ALTER SEQUENCE public.product_delivery_option_id_seq OWNED BY public.product_delivery_option.id;


--
-- Name: product_id_seq; Type: SEQUENCE; Schema: public; Owner: shop_admin
--

CREATE SEQUENCE public.product_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.product_id_seq OWNER TO shop_admin;

--
-- Name: product_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: shop_admin
--

ALTER SEQUENCE public.product_id_seq OWNED BY public.product.id;


--
-- Name: product_image; Type: TABLE; Schema: public; Owner: shop_admin
--

CREATE TABLE public.product_image (
    id integer NOT NULL,
    product_id integer NOT NULL,
    url character varying(255) NOT NULL
);


ALTER TABLE public.product_image OWNER TO shop_admin;

--
-- Name: product_image_id_seq; Type: SEQUENCE; Schema: public; Owner: shop_admin
--

CREATE SEQUENCE public.product_image_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.product_image_id_seq OWNER TO shop_admin;

--
-- Name: product_image_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: shop_admin
--

ALTER SEQUENCE public.product_image_id_seq OWNED BY public.product_image.id;


--
-- Name: product_option_value; Type: TABLE; Schema: public; Owner: shop_admin
--

CREATE TABLE public.product_option_value (
    id integer NOT NULL,
    product_id integer NOT NULL,
    game_option_value_id integer NOT NULL
);


ALTER TABLE public.product_option_value OWNER TO shop_admin;

--
-- Name: product_option_value_id_seq; Type: SEQUENCE; Schema: public; Owner: shop_admin
--

CREATE SEQUENCE public.product_option_value_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.product_option_value_id_seq OWNER TO shop_admin;

--
-- Name: product_option_value_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: shop_admin
--

ALTER SEQUENCE public.product_option_value_id_seq OWNED BY public.product_option_value.id;


--
-- Name: rel_delivery_option_game_service; Type: TABLE; Schema: public; Owner: shop_admin
--

CREATE TABLE public.rel_delivery_option_game_service (
    delivery_option_id integer NOT NULL,
    game_id integer NOT NULL,
    service_id integer NOT NULL
);


ALTER TABLE public.rel_delivery_option_game_service OWNER TO shop_admin;

--
-- Name: rel_game_platform; Type: TABLE; Schema: public; Owner: shop_admin
--

CREATE TABLE public.rel_game_platform (
    game_id integer NOT NULL,
    platform_id integer NOT NULL
);


ALTER TABLE public.rel_game_platform OWNER TO shop_admin;

--
-- Name: rel_service_game; Type: TABLE; Schema: public; Owner: shop_admin
--

CREATE TABLE public.rel_service_game (
    game_id integer NOT NULL,
    service_id integer NOT NULL,
    price_unit_placeholder character varying(16) DEFAULT ''::character varying NOT NULL,
    advanced_form_name character varying(32) NOT NULL
);


ALTER TABLE public.rel_service_game OWNER TO shop_admin;

--
-- Name: service; Type: TABLE; Schema: public; Owner: shop_admin
--

CREATE TABLE public.service (
    id integer NOT NULL,
    parent_id integer DEFAULT 0 NOT NULL,
    name character varying(32) NOT NULL,
    delivery_guidelines text DEFAULT ''::text NOT NULL
);


ALTER TABLE public.service OWNER TO shop_admin;

--
-- Name: service_duration; Type: TABLE; Schema: public; Owner: shop_admin
--

CREATE TABLE public.service_duration (
    id integer NOT NULL,
    service_id integer NOT NULL,
    duration_days integer NOT NULL
);


ALTER TABLE public.service_duration OWNER TO shop_admin;

--
-- Name: service_duration_id_seq; Type: SEQUENCE; Schema: public; Owner: shop_admin
--

CREATE SEQUENCE public.service_duration_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.service_duration_id_seq OWNER TO shop_admin;

--
-- Name: service_duration_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: shop_admin
--

ALTER SEQUENCE public.service_duration_id_seq OWNED BY public.service_duration.id;


--
-- Name: service_id_seq; Type: SEQUENCE; Schema: public; Owner: shop_admin
--

CREATE SEQUENCE public.service_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.service_id_seq OWNER TO shop_admin;

--
-- Name: service_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: shop_admin
--

ALTER SEQUENCE public.service_id_seq OWNED BY public.service.id;


--
-- Name: service_insurance; Type: TABLE; Schema: public; Owner: shop_admin
--

CREATE TABLE public.service_insurance (
    id integer NOT NULL,
    service_id integer NOT NULL,
    insurance_days integer NOT NULL
);


ALTER TABLE public.service_insurance OWNER TO shop_admin;

--
-- Name: service_insurance_id_seq; Type: SEQUENCE; Schema: public; Owner: shop_admin
--

CREATE SEQUENCE public.service_insurance_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.service_insurance_id_seq OWNER TO shop_admin;

--
-- Name: service_insurance_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: shop_admin
--

ALTER SEQUENCE public.service_insurance_id_seq OWNED BY public.service_insurance.id;


--
-- Name: shop; Type: TABLE; Schema: public; Owner: shop_admin
--

CREATE TABLE public.shop (
    id integer NOT NULL,
    owner_id integer NOT NULL,
    name character varying(255) NOT NULL,
    url character varying(255) NOT NULL,
    rating numeric(2,1) DEFAULT 0.0 NOT NULL,
    image character varying(255) DEFAULT 'store_images/default.jpeg'::character varying NOT NULL
);


ALTER TABLE public.shop OWNER TO shop_admin;

--
-- Name: shop_id_seq; Type: SEQUENCE; Schema: public; Owner: shop_admin
--

CREATE SEQUENCE public.shop_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.shop_id_seq OWNER TO shop_admin;

--
-- Name: shop_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: shop_admin
--

ALTER SEQUENCE public.shop_id_seq OWNED BY public.shop.id;


--
-- Name: shop_member; Type: TABLE; Schema: public; Owner: shop_admin
--

CREATE TABLE public.shop_member (
    shop_id integer NOT NULL,
    user_id integer NOT NULL,
    role character varying(255) DEFAULT ''::character varying NOT NULL,
    role_description text DEFAULT ''::text NOT NULL
);


ALTER TABLE public.shop_member OWNER TO shop_admin;

--
-- Name: shop_member_permission; Type: TABLE; Schema: public; Owner: shop_admin
--

CREATE TABLE public.shop_member_permission (
    shop_id integer NOT NULL,
    user_id integer NOT NULL,
    permission_id character varying(32) NOT NULL
);


ALTER TABLE public.shop_member_permission OWNER TO shop_admin;

--
-- Name: currency id; Type: DEFAULT; Schema: public; Owner: shop_admin
--

ALTER TABLE ONLY public.currency ALTER COLUMN id SET DEFAULT nextval('public.currency_id_seq'::regclass);


--
-- Name: delivery_option id; Type: DEFAULT; Schema: public; Owner: shop_admin
--

ALTER TABLE ONLY public.delivery_option ALTER COLUMN id SET DEFAULT nextval('public.delivery_option_id_seq'::regclass);


--
-- Name: game id; Type: DEFAULT; Schema: public; Owner: shop_admin
--

ALTER TABLE ONLY public.game ALTER COLUMN id SET DEFAULT nextval('public.game_id_seq'::regclass);


--
-- Name: game_option id; Type: DEFAULT; Schema: public; Owner: shop_admin
--

ALTER TABLE ONLY public.game_option ALTER COLUMN id SET DEFAULT nextval('public.game_option_id_seq'::regclass);


--
-- Name: game_option_value id; Type: DEFAULT; Schema: public; Owner: shop_admin
--

ALTER TABLE ONLY public.game_option_value ALTER COLUMN id SET DEFAULT nextval('public.game_option_value_id_seq'::regclass);


--
-- Name: platform id; Type: DEFAULT; Schema: public; Owner: shop_admin
--

ALTER TABLE ONLY public.platform ALTER COLUMN id SET DEFAULT nextval('public.platform_id_seq'::regclass);


--
-- Name: platform_group id; Type: DEFAULT; Schema: public; Owner: shop_admin
--

ALTER TABLE ONLY public.platform_group ALTER COLUMN id SET DEFAULT nextval('public.platform_group_id_seq'::regclass);


--
-- Name: product id; Type: DEFAULT; Schema: public; Owner: shop_admin
--

ALTER TABLE ONLY public.product ALTER COLUMN id SET DEFAULT nextval('public.product_id_seq'::regclass);


--
-- Name: product_delivery_option id; Type: DEFAULT; Schema: public; Owner: shop_admin
--

ALTER TABLE ONLY public.product_delivery_option ALTER COLUMN id SET DEFAULT nextval('public.product_delivery_option_id_seq'::regclass);


--
-- Name: product_image id; Type: DEFAULT; Schema: public; Owner: shop_admin
--

ALTER TABLE ONLY public.product_image ALTER COLUMN id SET DEFAULT nextval('public.product_image_id_seq'::regclass);


--
-- Name: product_option_value id; Type: DEFAULT; Schema: public; Owner: shop_admin
--

ALTER TABLE ONLY public.product_option_value ALTER COLUMN id SET DEFAULT nextval('public.product_option_value_id_seq'::regclass);


--
-- Name: service id; Type: DEFAULT; Schema: public; Owner: shop_admin
--

ALTER TABLE ONLY public.service ALTER COLUMN id SET DEFAULT nextval('public.service_id_seq'::regclass);


--
-- Name: service_duration id; Type: DEFAULT; Schema: public; Owner: shop_admin
--

ALTER TABLE ONLY public.service_duration ALTER COLUMN id SET DEFAULT nextval('public.service_duration_id_seq'::regclass);


--
-- Name: service_insurance id; Type: DEFAULT; Schema: public; Owner: shop_admin
--

ALTER TABLE ONLY public.service_insurance ALTER COLUMN id SET DEFAULT nextval('public.service_insurance_id_seq'::regclass);


--
-- Name: shop id; Type: DEFAULT; Schema: public; Owner: shop_admin
--

ALTER TABLE ONLY public.shop ALTER COLUMN id SET DEFAULT nextval('public.shop_id_seq'::regclass);


--
-- Data for Name: alembic_version; Type: TABLE DATA; Schema: public; Owner: shop_admin
--

COPY public.alembic_version (version_num) FROM stdin;
0001
\.


--
-- Data for Name: currency; Type: TABLE DATA; Schema: public; Owner: shop_admin
--

COPY public.currency (id, name, sign) FROM stdin;
1	USD	$
\.


--
-- Data for Name: delivery_option; Type: TABLE DATA; Schema: public; Owner: shop_admin
--

COPY public.delivery_option (id, name) FROM stdin;
7	Face to face
8	Аукцион
10	Login in
11	Почта
12	Прямая связь
13	Передача данных аккаунта
14	Self play
15	Trade
\.


--
-- Data for Name: game; Type: TABLE DATA; Schema: public; Owner: shop_admin
--

COPY public.game (id, name, description, url, image_url, chat_image_url) FROM stdin;
28	Test for Max	Какое-то описание	test_for_max	games/images/default.jpeg	games/chat_images/default.jpeg
29	Aion	Aion 	aion	games/images/default.jpeg	games/chat_images/default.jpeg
30	8 Ball Pool	Описание	8_ball_pool	games/images/default.jpeg	games/chat_images/default.jpeg
31	Age of Magic	Описание	age_of_magic	games/images/default.jpeg	games/chat_images/default.jpeg
32	ArcheAge BEGINS	Описание	archeage_begins	games/images/default.jpeg	games/chat_images/default.jpeg
34	Arena of Valor	Описание	arena_of_valor	games/images/default.jpeg	games/chat_images/default.jpeg
36	Albion Online	1	albion_online	games/images/default.jpeg	games/chat_images/default.jpeg
38	Army Men Strike	Описание	army_men_strike	games/images/default.jpeg	games/chat_images/default.jpeg
39	Asphalt 8: Airborne	Описание	asphalt_8_airborne	games/images/default.jpeg	games/chat_images/default.jpeg
40	Asphalt 9: Legends	Описание	asphalt_9_legends	games/images/default.jpeg	games/chat_images/default.jpeg
42	Assassin's Creed Rebellion	Описание	assassins_creed_rebellion	games/images/default.jpeg	games/chat_images/default.jpeg
43	Aurcus Online	Описание	aurcus_online	games/images/default.jpeg	games/chat_images/default.jpeg
44	Avabel Online	Описание	avabel_online	games/images/default.jpeg	games/chat_images/default.jpeg
45	Avakin Life	Описание	avakin_life	games/images/default.jpeg	games/chat_images/default.jpeg
46	AxE: Alliance vs Empire	Описание	axe_alliance_vs_empire	games/images/default.jpeg	games/chat_images/default.jpeg
47	Azar		azar	games/images/default.jpeg	games/chat_images/default.jpeg
48	Battle Bay	Описание	battle_bay	games/images/default.jpeg	games/chat_images/default.jpeg
49	Battle Warship:Naval Empire	Описание	battle_warshipnaval_empire	games/images/default.jpeg	games/chat_images/default.jpeg
50	Bigo Live - Live Stream, Live Video & Live Chat	Описание	bigo_live_live_stream_live_video_live_chat	games/images/default.jpeg	games/chat_images/default.jpeg
51	ArcheAge 	1	archeage	games/images/default.jpeg	games/chat_images/default.jpeg
52	Blade & Wings: 3D Fantasy Anime of Fate & Legends	Описание	blade_wings_3d_fantasy_anime_of_fate_legends	games/images/default.jpeg	games/chat_images/default.jpeg
53	BLEACH Brave Souls	Описание	bleach_brave_souls	games/images/default.jpeg	games/chat_images/default.jpeg
54	BLEACH Mobile 3D	Описание	bleach_mobile_3d	games/images/default.jpeg	games/chat_images/default.jpeg
55	Bomb Man SEA	Описание	bomb_man_sea	games/images/default.jpeg	games/chat_images/default.jpeg
56	Brave Frontier	Описание	brave_frontier	games/images/default.jpeg	games/chat_images/default.jpeg
58	Brave Frontier: The Last Summoner	Описание	brave_frontier_the_last_summoner	games/images/default.jpeg	games/chat_images/default.jpeg
59	Brawl Stars	Описание	brawl_stars	games/images/default.jpeg	games/chat_images/default.jpeg
60	Brutal Age: Horde Invasion	Описание	brutal_age_horde_invasion	games/images/default.jpeg	games/chat_images/default.jpeg
61	Captain Tsubasa: Dream Team	Описание	captain_tsubasa_dream_team	games/images/default.jpeg	games/chat_images/default.jpeg
62	Castle Burn - RTS Revolution	Описание	castle_burn_rts_revolution	games/images/default.jpeg	games/chat_images/default.jpeg
63	Castle Clash	Описание	castle_clash	games/images/default.jpeg	games/chat_images/default.jpeg
65	CATS: Crash Arena Turbo Stars	Описание	cats_crash_arena_turbo_stars	games/images/default.jpeg	games/chat_images/default.jpeg
66	Choices: Stories You Play	Описание	choices_stories_you_play	games/images/default.jpeg	games/chat_images/default.jpeg
68	Clash of Clans	Описание	clash_of_clans	games/images/default.jpeg	games/chat_images/default.jpeg
69	Clash Of Kings	Описание	clash_of_kings	games/images/default.jpeg	games/chat_images/default.jpeg
70	Clash Royale	Описание	clash_royale	games/images/default.jpeg	games/chat_images/default.jpeg
71	Command & Conquer: Rivals™ PVP	Описание	command_conquer_rivalstm_pvp	games/images/default.jpeg	games/chat_images/default.jpeg
72	Crisis Action - Enjoy Pure FPS Here	Описание	crisis_action_enjoy_pure_fps_here	games/images/default.jpeg	games/chat_images/default.jpeg
73	Critical Ops	Описание	critical_ops	games/images/default.jpeg	games/chat_images/default.jpeg
74	CrossFire: Legends	Описание	crossfire_legends	games/images/default.jpeg	games/chat_images/default.jpeg
75	Crusaders of Light	Описание	crusaders_of_light	games/images/default.jpeg	games/chat_images/default.jpeg
76	CSR Racing 2	Описание	csr_racing_2	games/images/default.jpeg	games/chat_images/default.jpeg
77	Darkness Rises	Описание	darkness_rises	games/images/default.jpeg	games/chat_images/default.jpeg
78	Dawn of Titans - Epic War Strategy Game	Описание	dawn_of_titans_epic_war_strategy_game	games/images/default.jpeg	games/chat_images/default.jpeg
79	Daybreak Legends: Origin	Описание	daybreak_legends_origin	games/images/default.jpeg	games/chat_images/default.jpeg
80	DC Legends: Battle for Justice	Описание	dc_legends_battle_for_justice	games/images/default.jpeg	games/chat_images/default.jpeg
81	Design Home	Описание	design_home	games/images/default.jpeg	games/chat_images/default.jpeg
82	Digital Master	Описание	digital_master	games/images/default.jpeg	games/chat_images/default.jpeg
83	Digital World-Adventure	Описание	digital_worldadventure	games/images/default.jpeg	games/chat_images/default.jpeg
84	Dino War	Описание	dino_war	games/images/default.jpeg	games/chat_images/default.jpeg
85	DISSIDIA FINAL FANTASY OPERA OMNIA	Описание	dissidia_final_fantasy_opera_omnia	games/images/default.jpeg	games/chat_images/default.jpeg
86	DOFUS Touch	Описание	dofus_touch	games/images/default.jpeg	games/chat_images/default.jpeg
87	DomiNations	Описание	dominations	games/images/default.jpeg	games/chat_images/default.jpeg
88	Dragalia Lost	Описание	dragalia_lost	games/images/default.jpeg	games/chat_images/default.jpeg
89	DRAGON BALL LEGENDS	Описание	dragon_ball_legends	games/images/default.jpeg	games/chat_images/default.jpeg
90	Dragon City	Описание	dragon_city	games/images/default.jpeg	games/chat_images/default.jpeg
91	Dragon Nest M	Описание	dragon_nest_m	games/images/default.jpeg	games/chat_images/default.jpeg
92	Dragons of Atlantis	Описание	dragons_of_atlantis	games/images/default.jpeg	games/chat_images/default.jpeg
93	Dungeon Hunter 5 – Action RPG	Описание	dungeon_hunter_5_action_rpg	games/images/default.jpeg	games/chat_images/default.jpeg
94	Dungeon Rush: Rebirth	Описание	dungeon_rush_rebirth	games/images/default.jpeg	games/chat_images/default.jpeg
95	Dunk Nation 3X3	Описание	dunk_nation_3x3	games/images/default.jpeg	games/chat_images/default.jpeg
96	Dynasty War - Hero Clash	Описание	dynasty_war_hero_clash	games/images/default.jpeg	games/chat_images/default.jpeg
97	ARK: Survival Evolved	1	ark_survival_evolved	games/images/default.jpeg	games/chat_images/default.jpeg
98	Black Desert	1	black_desert	games/images/default.jpeg	games/chat_images/default.jpeg
100	Bless Online	1	bless_online	games/images/default.jpeg	games/chat_images/default.jpeg
102	Emperor And Beauties	Описание	emperor_and_beauties	games/images/default.jpeg	games/chat_images/default.jpeg
103	Empires & Puzzles: RPG Quest	Описание	empires_puzzles_rpg_quest	games/images/default.jpeg	games/chat_images/default.jpeg
104	Epic Seven	Описание	epic_seven	games/images/default.jpeg	games/chat_images/default.jpeg
105	Era of Destiny	Описание	era_of_destiny	games/images/default.jpeg	games/chat_images/default.jpeg
106	Errant: Hunter's Soul	Описание	errant_hunters_soul	games/images/default.jpeg	games/chat_images/default.jpeg
107	Eternal Fury 2 - Become Proficient in Mercenary	Описание	eternal_fury_2_become_proficient_in_mercenary	games/images/default.jpeg	games/chat_images/default.jpeg
108	EvilBane: Rise of Ravens	Описание	evilbane_rise_of_ravens	games/images/default.jpeg	games/chat_images/default.jpeg
109	Evony: The King's Return	Описание	evony_the_kings_return	games/images/default.jpeg	games/chat_images/default.jpeg
99	Blade & Soul	1	blade_soul	games/images/default.jpeg	games/chat_images/default.jpeg
110	Fate/Grand Order	Описание	fategrand_order	games/images/default.jpeg	games/chat_images/default.jpeg
111	FIFA Mobile	Описание	fifa_mobile	games/images/default.jpeg	games/chat_images/default.jpeg
112	Final Contract: Legacy of War - SEA	Описание	final_contract_legacy_of_war_sea	games/images/default.jpeg	games/chat_images/default.jpeg
113	Final Fantasy Awakening: SE Licensed	Описание	final_fantasy_awakening_se_licensed	games/images/default.jpeg	games/chat_images/default.jpeg
114	Final Fantasy XV: A New Empire	Описание	final_fantasy_xv_a_new_empire	games/images/default.jpeg	games/chat_images/default.jpeg
115	FINAL FANTASY BRAVE EXVIUS	Описание	final_fantasy_brave_exvius	games/images/default.jpeg	games/chat_images/default.jpeg
116	Final Fighter	Описание	final_fighter	games/images/default.jpeg	games/chat_images/default.jpeg
117	Fire Emblem Heroes	Описание	fire_emblem_heroes	games/images/default.jpeg	games/chat_images/default.jpeg
118	Football Master	Описание	football_master	games/images/default.jpeg	games/chat_images/default.jpeg
119	Football Strike - Multiplayer Soccer	Описание	football_strike_multiplayer_soccer	games/images/default.jpeg	games/chat_images/default.jpeg
41	Apex Legends	123	apex_legends	games/images/default.jpeg	games/chat_images/default.jpeg
120	Counter-Strike: GO	123	counterstrike_go	games/images/default.jpeg	games/chat_images/default.jpeg
\.


--
-- Data for Name: game_option; Type: TABLE DATA; Schema: public; Owner: shop_admin
--

COPY public.game_option (id, game_id, service_id, game_option_name) FROM stdin;
10	28	22	Фракция
11	29	23	Сервер
12	29	23	Сторона
13	29	25	Сервер
14	29	25	Сторона
15	29	25	Класс
16	29	26	Прокачка уровня
17	29	26	Сервер
18	29	26	Сторона
19	30	22	Cash
20	31	22	Golds
21	32	22	Gems
23	34	22	Voucher
24	36	23	Сервер
25	36	25	Сервер
26	36	24	Сервер
27	36	26	Прокачка уровня
28	36	26	PVP
29	36	26	PVE
31	38	22	Golds
32	39	22	Credits
33	40	22	Tokens
39	42	22	Helix
40	43	22	Coins
41	44	22	Gems
42	45	22	Avacoins
43	46	22	Diamonds
44	47	22	Gems
45	48	22	Pearls
46	49	22	Gold Coins
47	50	22	Diamonds
48	51	23	Сервер
49	51	24	Сервер
50	51	26	Сервер
51	51	25	Сервер
52	52	22	Gems
53	53	22	Orbs
54	54	22	Crystals
55	55	22	Diamonds
56	56	22	Gems
57	58	22	Orbs
58	59	22	Gems
59	60	22	Gems
60	61	22	Dreamballs
61	62	22	Rubies
62	63	22	Gems
63	65	22	Gems
64	66	22	Diamonds
65	68	22	Gems
66	69	22	Golds
67	70	22	Gems
68	71	22	Diamonds
69	72	22	Diamonds
70	73	22	Credits
71	74	22	Gems
72	75	22	Crystals
73	76	22	Golds
74	77	22	Gems
75	78	22	Gems
76	79	22	Diamonds
77	80	22	Gems
78	81	22	Diamonds
79	82	22	Diamonds
80	83	22	Diamonds
81	84	22	Diamonds
82	85	22	Gems
83	86	22	Goultines
84	87	22	Crowns
85	88	22	Diamantium
86	89	22	Chrono Crystals
87	90	22	Gems
88	91	22	Diamonds
89	92	22	Rubies
90	93	22	Gems
91	94	22	Gems
92	95	22	Gems
93	96	22	Ingots
94	97	24	Сервер
95	97	26	Сервер
96	97	25	Сервер
97	98	23	Сервер
98	98	24	Экипировка
99	98	24	Рабочие
100	98	25	Сервер
101	98	25	Класс
102	98	26	Типы буста
103	98	26	Сервер
104	98	26	Тип буста
108	100	23	Сервер
109	100	25	Сервер
110	102	22	Ingots
111	103	22	Gems
112	104	22	Skystones
113	105	22	Diamonds
114	106	22	Soul Gems
115	107	22	Balens
116	108	22	Crystals
117	109	22	Gems
118	110	22	Saint Quartez
119	111	22	Fifa Points
120	112	22	Diamonds
121	113	22	Diamonds
122	114	22	Golds
123	115	22	Lapis
124	116	22	Gems
125	117	22	Orbs
126	118	22	Gems
127	119	22	Cash
38	41	26	Другое
105	99	23	Сервер
106	99	25	Сервер
107	99	25	Класс
128	99	26	Powerleveling
129	99	26	PVP arena
130	99	26	PVE 
131	99	26	Achievement
132	99	26	Others
34	41	26	Прокачка уровней
35	41	26	Победы
36	41	26	Достижения
37	41	26	Убийства
133	120	34	Тип Винтовки
136	120	34	Категория
143	120	31	Оформление
144	120	31	Категория
141	120	31	Редкость
145	120	33	Тип
146	120	33	Оформление
142	120	31	Тип Пистолета
147	120	33	Категория
148	120	33	Редкость
149	120	32	ТИП
150	120	32	Оформление
151	120	32	Категория
152	120	32	Редкость
153	120	35	ТИП
155	120	35	Категория
156	120	35	Редкость
157	120	36	Тип
158	120	36	Оформление 
159	120	36	Категория 
135	120	34	Редкость
134	120	34	Оформление
154	120	35	Оформление
160	120	36	Редкость
161	120	37	Тип
162	120	37	Оформление 
163	120	37	Категория 
164	120	37	Редкость 
165	120	38	Тип
166	120	38	Оформление 
167	120	38	Категория 
168	120	38	Редкость  
169	120	27	Тип
\.


--
-- Data for Name: game_option_value; Type: TABLE DATA; Schema: public; Owner: shop_admin
--

COPY public.game_option_value (id, game_option_id, game_option_value) FROM stdin;
41	10	Орда
42	10	Альянс
43	11	[DE] Odin
44	11	[EN] Sillus
45	11	[EN] Stormwing
46	11	[EN] Union 1
47	11	[EN] Union 2
48	11	[FR] Ragnarok
49	11	[NA] Danaria
50	11	[NA] Ereshkigal
51	11	[NA] Katalam
52	12	Асмодиане
53	12	Элийцы
54	13	[DE] Odin
55	13	[EN] Sillus
56	13	[EN] Stormwing
57	13	[EN] Union 1
58	13	[EN] Union 2
59	13	[FR] Ragnarok
60	13	[NA] Danaria
61	13	[NA] Ereshkigal
62	13	[NA] Katalam
63	14	Асмодиане
64	14	Элийцы
65	15	Убийца
66	15	Стрелок
67	15	Гладиатор
68	15	Страж
69	15	Волшебник
70	15	Заклинатель
71	15	Целитель
72	15	Чародей
73	15	Снайпер
74	15	Бард
75	15	Пилот
76	15	Художник
77	16	1-80
78	17	[DE] Odin
79	17	[EN] Sillus
80	17	[EN] Stormwing
81	17	[EN] Union 1
82	17	[EN] Union 2
83	17	[FR] Ragnarok
84	17	[NA] Danaria
85	17	[NA] Ereshkigal
86	17	[NA] Katalam
87	18	Асмодиане
88	18	Элийцы
89	19	25
90	19	55
91	19	125
92	19	400
93	19	1000
94	19	Others
95	20	1340
96	20	2800
97	20	7370
98	20	15710
99	20	Others
100	21	500
101	21	2500
102	21	5000
103	21	Others
110	23	500
111	23	1200
112	23	2500
113	23	6500
114	23	14000
115	23	Others
116	24	Основной сервер
117	25	Основной сервер
118	26	Основной сервер
119	27	1-8 ранг
126	31	700
127	31	1600
128	31	3400
129	31	9000
130	31	21000
131	31	Others
132	32	16000
133	32	35000
134	32	84000
135	32	240000
136	32	600000
137	32	Others
138	33	220
139	33	470
140	33	1275
141	33	3000
142	33	Others
144	39	1050
145	39	2200
146	39	6000
147	39	13000
148	39	Others
149	40	2000
150	40	3500
151	40	7200
152	40	15300
153	40	Others
154	41	6
155	41	12
156	41	27
157	41	69
158	41	140
159	41	Others
160	42	7600
161	42	32100
162	42	49000
163	42	100000
164	42	Others
165	43	175
166	43	550
167	43	955
168	43	1980
169	43	Others
170	44	1200
171	44	2500
172	44	6500
173	44	14000
174	44	33000
175	44	Others
176	45	500
177	45	1200
178	45	2500
179	45	6500
180	45	14000
181	45	Others
182	46	1000
183	46	2000
184	46	4400
185	46	12000
186	46	26000
187	46	Others
188	47	297
189	47	850
190	47	2552
191	47	4288
192	47	Others
193	48	[EU] Ezi
194	48	[EU] Jakar
195	48	[EU] Taris
196	48	[NA] Aria
197	48	[NA] Kadum
198	48	[NA] Nui
199	48	[RU] Ария
200	48	[RU] Каиль
201	48	[RU] Корвус
202	48	[RU] Луций
203	48	[RU] Нуи
204	48	[RU] Ренессанс
205	48	[RU] Фанем
206	48	[RU] Хазе
207	48	[RU] Шаеда
208	49	[EU] Ezi
209	49	[EU] Jakar
210	49	[EU] Taris
211	49	[NA] Aria
212	49	[NA] Kadum
213	49	[NA] Nui
214	49	[RU] Ария
215	49	[RU] Каиль
216	49	[RU] Корвус
217	49	[RU] Луций
218	49	[RU] Нуи
219	49	[RU] Ренессанс
220	49	[RU] Фанем
221	49	[RU] Хазе
222	49	[RU] Шаеда
223	50	[EU] Ezi
224	50	[EU] Jakar
225	50	[EU] Taris
226	50	[NA] Aria
227	50	[NA] Kadum
228	50	[NA] Nui
229	50	[RU] Ария
230	50	[RU] Каиль
231	50	[RU] Корвус
232	50	[RU] Луций
233	50	[RU] Нуи
234	50	[RU] Ренессанс
235	50	[RU] Фанем
236	50	[RU] Хазе
237	50	[RU] Шаеда
238	51	[EU] Ezi
239	51	[EU] Jakar
240	51	[EU] Taris
241	51	[NA] Aria
242	51	[NA] Kadum
243	51	[NA] Nui
244	51	[RU] Ария
245	51	[RU] Каиль
246	51	[RU] Корвус
247	51	[RU] Луций
248	51	[RU] Нуи
249	51	[RU] Ренессанс
250	51	[RU] Фанем
251	51	[RU] Хазе
252	51	[RU] Шаеда
253	52	60
254	52	680
255	52	1680
256	52	3280
257	52	6480
258	52	Others
259	53	6
260	53	24
261	53	48
262	53	70
263	53	145
264	53	240
265	53	490
266	53	Others
267	54	1530
268	54	1980
269	54	2970
270	54	4950
271	54	9000
272	54	Others
273	55	600
274	55	1200
275	55	1800
276	55	3000
277	55	6000
278	55	Others
279	56	12
280	56	24
281	56	50
282	56	100
283	56	Others
284	57	620
285	57	1290
286	57	3590
287	57	8020
288	57	Others
289	58	30
290	58	80
291	58	170
292	58	360
293	58	950
294	58	2000
295	58	Others
296	59	660
297	59	1500
298	59	3300
299	59	9600
300	59	24000
301	59	Others
302	60	30
303	60	60
304	60	80
305	60	175
306	60	Others
307	61	1200; 2500; 7300; 16000
308	61	2500
309	61	7300
310	61	16000
311	61	Others
312	62	600
313	62	1400
314	62	3000
315	62	7800
316	62	16800
317	62	Others
318	63	600
319	63	1500
320	63	5000
321	63	12000
322	63	Others
323	64	30
324	64	150
325	64	250
326	64	550
327	64	1500
328	64	Others
329	65	500
330	65	1200
331	65	2500
332	65	6500
333	65	14000
334	65	Others
335	66	700
336	66	1600
337	66	3400
338	66	9000
339	66	21000
340	66	Others
341	67	500
342	67	1200
343	67	2500
344	67	6500
345	67	14000
346	68	600
347	68	1250
348	68	2600
349	68	7000
350	68	15000
351	68	Others
352	69	300
353	69	600
354	69	1200
355	69	3000
356	69	6000
357	69	Others
358	70	400
359	70	1400
360	70	3000
361	70	10000
362	70	Others
363	71	430
364	71	860
365	71	1280
366	71	2250
367	71	5100
368	71	Others
369	72	50
370	72	100
371	72	200
372	72	500
373	72	1000
374	72	Others
375	73	1100
376	73	1750
377	73	2500
378	73	16500
379	73	Others
380	74	1050
381	74	3300
382	74	5750
383	74	12000
384	74	Others
385	75	2000
386	75	4500
387	75	10000
388	75	27000
389	75	60000
390	75	Others
391	76	900
392	76	1200
393	76	1800
394	76	3000
395	76	6000
396	76	Others
397	77	1000
398	77	2200
399	77	4600
400	77	12000
401	77	25000
402	77	Others
403	78	16500
404	78	34500
405	78	90000
406	78	187500
407	78	Others
408	79	600
409	79	980
410	79	1980
411	79	3280
412	79	6480
413	79	Others
414	80	300
415	80	600
416	80	980
417	80	1980
418	80	3280
419	80	6480
420	80	Others
421	81	5000
422	81	10000
423	81	20000
424	81	50000
425	81	100000
426	81	Others
427	82	700
428	82	1500
429	82	3000
430	82	6000; 
431	82	12000
432	82	Others
433	83	2400
434	83	7500
435	83	16500
436	83	31000
437	83	45000
438	83	Others
439	84	1200
440	84	2500
441	84	6500
442	84	14000
443	84	Others
444	85	484
445	85	980
446	85	1560
447	85	2000
448	85	4200
449	85	Others
450	86	310
451	86	1100
452	86	2000
453	86	Others
454	87	140
455	87	300
456	87	465
457	87	800
458	87	1700
459	87	Others
460	88	450
461	88	900
462	88	1800
463	88	4500
464	88	9000
465	88	Others
466	89	50
467	89	100
468	89	375
469	89	665
470	89	1600
471	89	Others
472	90	430
473	90	888
474	90	2400
475	90	5500
476	90	Others
477	91	50
478	91	100
479	91	200
480	91	500
481	91	1000
482	91	Others
483	92	600
484	92	1200
485	92	3000
486	92	6000
487	92	Others
488	93	380
489	93	1400
490	93	3300
491	93	7000
492	93	Others
493	94	PVE Legacy
494	94	PVE
495	94	PVP
496	94	PVP Legacy
497	95	PVE
498	95	PVE Legacy
499	95	PVP
500	95	PVP Legacy
501	96	PVE
502	96	PVE Legacy
503	96	PVP
504	96	PVP Legacy
505	97	EU
506	97	NA
507	97	SA
508	97	SEA
509	97	Ru
510	97	MENA
511	97	Taiwan
512	97	Thailand
513	98	Бижутерия
514	98	Шлем
515	98	Доспех
516	98	Ботинки
517	98	Перчатки
518	98	Оружие (основное)
519	98	Пробужденное оружие
520	98	Доп. Оружие
521	98	Внешний Вид
522	100	EU
523	100	NA
524	100	SA
525	100	SEA
526	100	RU
527	100	MENA
528	100	Thailand
529	100	Taiwan
530	101	Маэва
531	101	Колдунья
532	101	Лан
533	101	Лучница
534	101	Варва
535	101	Мистик
536	101	Мастер Меча
537	101	Валькирия
538	101	Волшебник
539	101	Волшебница
540	101	Воин
541	101	Ниндзя
542	101	Куноичи
543	101	Темный рыцарь
544	101	Боец
545	101	Фурия
546	101	Лучник
547	101	Шай
548	102	Прокачка уровня
549	102	Фарм
550	102	Прокачка профессии
551	102	Выполнение квестов
552	103	EU
553	103	NA
554	103	SA
555	103	SEA
556	103	RU
557	103	Taiwan
558	103	MENA
559	103	Thailand
560	104	Играете сами
561	104	Передача аккаунта
563	105	EU
564	105	NA
565	106	RU
566	106	EU
567	106	NA
568	107	Assasin
569	107	Blade Dancer
570	107	Blade Master
580	108	EU
581	108	NA
582	109	EU
583	109	NA
584	110	600
585	110	1200
586	110	3000
587	110	6000
588	110	Others
589	111	800
590	111	1800
591	111	2800
592	111	4800
593	111	10000
594	111	Others
595	112	300
596	112	900
597	112	1500
598	112	3000
599	112	Others
600	113	600
601	113	1200
602	113	3200
603	113	6480
604	113	Others
605	114	980
606	114	3180
607	114	5980
608	114	Others
609	115	1000
610	115	2000
611	115	3000
612	115	5000
613	115	10000
614	115	Others
615	116	52
616	116	105
617	116	330
618	116	570
619	116	1180
620	116	Others
621	117	1000
622	117	2000
623	117	5000
624	117	10000
625	117	25000
626	117	Others
627	118	18
628	118	41
629	118	76
630	118	167
631	118	Others
632	119	500
633	119	1050
634	119	2200
635	119	5750
636	119	12000
637	119	Others
638	120	300
639	120	900
640	120	1800
641	120	3000
642	120	6000
643	120	Others
644	121	525
645	121	1080
646	121	2200
647	121	5600
648	121	11500
649	121	Others
650	122	600
651	122	3000
652	122	20000
653	122	Others
654	123	840+160
655	123	1200+300
656	123	2400+800
657	123	6000+2500
658	123	12000+6000
659	123	Others
660	124	980
661	124	1980
662	124	3280
663	124	6480
664	124	Others
665	125	23
666	125	35
667	125	48
668	125	75
669	125	140
670	125	Others
671	126	300
672	126	620
673	126	750
674	126	1500
675	126	2700
676	126	5500
677	126	Others
678	127	520
679	127	1100
680	127	2500
681	127	8000
682	127	20000
683	127	Others
562	105	RU
571	107	Destroyer
572	107	Force Master
573	107	Gunslinger
574	107	Kung Fu Master
575	107	Soul Fighter
576	107	Summoner
577	107	Warden
578	107	Warlock
579	107	Zen Archer
684	128	Character level
685	128	Hongmoon level
686	128	Others
687	130	Dungeon
688	130	Quest
689	130	Others
143	34	1-100
695	133	SG 553
717	133	GALIL AR
690	133	FAMAS
691	133	AK-48
692	133	M4A4
700	134	Прямо с завода
718	134	Не покрашено
698	134	Закаленное в боях
702	135	Армейское качество
704	135	Запрещённое
705	135	Засекреченное
706	135	Тайное
707	135	базового класса
708	135	экстраординарного типа
709	135	высшего класса
710	135	примечательного типа
711	135	экзотичного вида
712	135	Заслуженный
713	135	Контрабанда
714	135	Исключительный
715	135	Превосходный
716	135	Мастерский
701	135	Ширпотреб
693	133	M4A4-S
694	133	AUG
699	134	Поношенное
722	136	★ StatTrak™
703	135	Промышленное качество 
720	136	StatTrak™
721	136	★
719	136	Обычный
697	134	Немного поношенное
825	148	экзотичного вида
826	148	Заслуженный 
827	148	Контрабанда 
828	148	Исключительный 
829	148	Превосходный 
830	148	Мастерский
831	149	XM1014
832	149	SAWED-OFF
833	149	NOVA
834	149	MAG-7
835	150	После полевых испытаний 
836	150	Немного поношенное
837	150	Закаленное в боях
789	143	Поношенное 
790	143	Прямо с завода
791	143	Не покрашено
792	144	Обычный
793	144	 StatTrak™
794	144	Сувенирный 
795	144	★
796	144	★ StatTrak™
797	145	UMP-45
798	145	MAC-10
799	145	MP7
800	145	MP5-SD
801	145	P90
802	145	MP9
803	145	PP-BIZON
804	146	После полевых испытаний 
805	146	Немного поношенное
806	146	Закаленное в боях
807	146	Поношенное 
808	146	Прямо с завода
809	146	Не покрашено
810	147	Обычный 
811	147	 StatTrak™ 
812	147	Сувенирный 
813	147	★ 
814	147	★ StatTrak™
815	148	Ширпотреб 
816	148	Армейское качество 
817	148	Промышленное качество 
818	148	Запрещённое 
696	134	После полевых испытаний
773	141	Исключительный 
774	141	Превосходный 
775	141	Мастерский
760	141	Ширпотреб  
761	141	Армейское качество
762	141	Промышленное качество
763	141	Запрещённое 
764	141	Засекреченное 
765	141	Тайное 
766	141	базового класса
767	141	экстраординарного типа
768	141	высшего класса
769	141	примечательного типа
770	141	экзотичного вида
771	141	Заслуженный 
772	141	Контрабанда 
776	142	FIVE-SEVEN
777	142	TEC-9
778	142	CZ75-AUTO
779	142	DESERT EAGLE
780	142	P250
781	142	R8 REVOLVER
782	142	P2000
783	142	DUAL BERETTAS
784	142	GLOCK-18
785	142	USP-S
786	143	После полевых испытаний 
787	143	Немного поношенное
788	143	Закаленное в боях
819	148	Засекреченное 
820	148	Тайное 
821	148	базового класса
822	148	экстраординарного типа
823	148	 высшего класса
824	148	примечательного типа
840	150	Не покрашено
841	151	Обычный 
842	151	StatTrak™
843	151	Сувенирный 
844	151	★ 
845	151	★ StatTrak™
847	152	Тайное 
848	152	Армейское качество
849	152	Промышленное качество 
850	152	Запрещённое 
851	152	Засекреченное Тайное
852	152	базового класса
853	152	экстраординарного типа
854	152	высшего класса
855	152	примечательного типа
857	152	Заслуженный 
858	152	Контрабанда 
859	152	Исключительный 
860	152	Превосходный 
861	152	Мастерский
862	153	AWP
863	153	SCAR-20
864	153	SSG-08
865	153	G3SG1
866	154	После полевых испытаний 
867	154	Немного поношенное
868	154	Закаленное в боях
869	154	Поношенное 
870	154	Прямо с завода
871	154	 Не покрашено
872	155	Обычный 
873	155	StatTrak™
874	155	Сувенирный 
875	155	★
876	155	★ StatTrak™
877	156	Ширпотреб 
879	156	Промышленное качество
880	156	Запрещённое 
881	156	Засекреченное
882	156	Тайное 
883	156	базового класса
884	156	экстраординарного типа
885	156	 высшего класса
886	156	примечательного типа
887	156	 экзотичного вида
888	156	Заслуженный 
889	156	Контрабанда 
890	156	Исключительный 
891	156	Превосходный 
892	156	Мастерский
893	157	M249
894	157	NEGEV
896	158	Немного поношенное
897	158	Закаленное в боях
898	158	Поношенное 
899	158	Прямо с завода
900	158	Не покрашено
901	159	Обычный 
902	159	StatTrak™
903	159	Сувенирный 
904	159	★
905	159	★ StatTrak™
906	160	Ширпотреб 
907	160	 Армейское качество
908	160	 Промышленное качество 
909	160	Запрещённое 
910	160	Засекреченное 
911	160	Тайное 
912	160	базового класса
914	160	высшего класса
915	160	примечательного типа
916	160	экзотичного вида 
917	160	Заслуженный 
918	160	Контрабанда 
919	160	Исключительный 
920	160	Превосходный 
921	160	Мастерский
922	161	FACHION KNIFE
923	161	STILETTO KNIFE
924	161	GUT KNIFE
925	161	BUTTERFLY KNIFE
926	161	BAYONET
927	161	M9 BAYONET
928	161	TALON KNIFE
929	161	SHADOW DAGGERS
930	161	IRSUS KNIFE
931	161	HUNTSMAN KNIFE
932	161	FLIP KNIFE
933	161	BOWIE KNIFE
934	161	KARAMBIT
936	162	Немного поношенное
937	162	Закаленное в боях
938	162	Поношенное 
939	162	Прямо с завода 
940	162	Не покрашено
941	163	Обычный 
942	163	 StatTrak™
943	163	Сувенирный
944	163	★
945	163	★ StatTrak™
946	164	Ширпотреб 
947	164	Армейское качество
948	164	Промышленное качество
949	164	Запрещённое 
950	164	Засекреченное 
951	164	Тайное 
952	164	базового класса 
953	164	экстраординарного типа
954	164	высшего класса
955	164	примечательного типа
956	164	экзотичного вида 
957	164	Заслуженный 
958	164	Контрабанда 
960	164	Превосходный 
961	164	Мастерский
962	165	SPECIALIST GLOVES
963	165	DRIVER GLOVES
964	165	BLOODHOUND GLOVES
965	165	DEFAULT CT GLOVES
966	165	HYDRA GLOVES
967	165	DEFAULT T GLOVES
839	150	Прямо с завода
998	169	Наклейки
999	169	Граффити
1000	169	Ключи
838	150	Поношенное 
846	152	Ширпотреб 
856	152	экзотичного вида 
878	156	Армейское качество
895	158	 После полевых испытаний
913	160	экстраординарного типа
935	162	После полевых испытаний
959	164	Исключительный 
968	165	MOTO GLOVES
969	165	SPORT GLOVES
970	166	После полевых испытаний
971	166	Немного поношенное
972	166	Закаленное в боях 
973	166	Поношенное 
974	166	Прямо с завода
975	166	 Не покрашено
976	167	Обычный 
977	167	StatTrak™
978	167	Сувенирный 
979	167	 ★
980	167	★ StatTrak™
981	168	Ширпотреб 
982	168	Армейское качество
983	168	Промышленное качество
984	168	Запрещённое 
985	168	Засекреченное 
986	168	Тайное 
987	168	базового класса
988	168	экстраординарного типа
989	168	высшего класса
990	168	примечательного типа 
991	168	экзотичного вида
992	168	Заслуженный 
993	168	Контрабанда 
994	168	Исключительный 
995	168	Превосходный 
996	168	Мастерский
997	169	Контейнеры
\.


--
-- Data for Name: language; Type: TABLE DATA; Schema: public; Owner: shop_admin
--

COPY public.language (lang_code, lang_name) FROM stdin;
ru	Russian
en	English
ua	Ukrainian 
\.


--
-- Data for Name: localization; Type: TABLE DATA; Schema: public; Owner: shop_admin
--

COPY public.localization (text_code, lang_code, text_value) FROM stdin;
placeholder:enter_message	ru	Введите сообщение
placeholder:enter_message	en	Enter message
link:sign_up	ru	Регистрация
link:sign_up	en	Sign up
link:sign_up	ua	Реєстрація 
title:games	ru	Игры
title:games	en	Games
title:games	ua	Ігри
link:forum	ru	Форум
link:forum	en	Forum
link:forum	ua	Форум
link:sign_in	ru	Вход
link:sign_in	en	Sign in
link:sign_in	ua	Вхід
title:about_us	ru	О нас
title:about_us	en	About us
title:about_us	ua	Про нас
title:faq	ru	ЧаВо
title:faq	en	FAQ
title:faq	ua	ЧаПи
text:categories	ru	Категории
text:categories	en	Сategories
text:categories	ua	Категорії
placeholder:search	ru	Поиск
placeholder:search	en	Search
placeholder:search	ua	Пошук
title:welcome_header	ru	Добро пожаловать в Gaming Warehouse
title:welcome_header	en	Welcome to Gaming Warehouse
title:welcome_header	ua	Ласкаво просимо у Gaming Warehouse
title:welcome_text	ru	Добро пожаловать текст
title:welcome_text	en	Добро пожаловать текст
title:welcome_text	ua	Добро пожаловать текст
title:for_buyers	ru	Для покупателей
title:for_buyers	en	For buyers
title:for_buyers	ua	Для покупців
title:for_sellers	ru	Для Продавцов
title:for_sellers	en	For Buyers
title:for_sellers	ua	Для продавців
title:trade_protect	ru	Защита сделок
title:trade_protect	en	Trade Protection
title:trade_protect	ua	Захист угод
title:choose_platform	ru	Выберите платформу
title:choose_platform	en	Choose platform
title:choose_platform	ua	Виберіть платформу
title:mobile_games	ru	Мобильные игры
title:mobile_games	en	Mobile games
title:mobile_games	ua	Мобільні ігри
title:pc_games	ru	ПК Игры
title:pc_games	en	PC games
title:pc_games	ua	ПК ігри
title:console_games	ru	Консольные игры
title:console_games	en	Console games
title:console_games	ua	Консольні ігри
title:all_games	ru	Все игры
title:all_games	en	All games
title:all_games	ua	Усі ігри
title:recommendations	ru	Рекомендации
title:recommendations	en	Recommendations
title:recommendations	ua	Рекомендації
title:chat_general	ru	Общий чат
title:chat_general	en	General chat
title:chat_general	ua	Спільний чат
placeholder:search_by_games	ru	Поиск по играм
placeholder:search_by_games	en	Search by games
placeholder:search_by_games	ua	Пошук за іграми
placeholder:enter_message	ua	Введіть повідомлення
title:send	ru	Отправить
title:send	en	Send
title:send	ua	Відправити
title:payment_methods	ru	Способы оплаты
title:payment_methods	en	Payment methods
title:payment_methods	ua	Способи оплати
title:main -	ru	Главная страница
title:main -	en	Main page
title:main -	ua	Головна сторінка
link:start_shopping	ru	Перейти к покупкам
link:start_shopping	en	Start shopping
link:start_shopping	ua	Перейти до покупок
link:start_selling	ru	Начать  продавать
link:start_selling	en	Start selling
link:start_selling	ua	Почати продавати
text:designed_by: uix.digital	ru	Designed by: uix.digital
text:designed_by: uix.digital	en	Designed by: uix.digital
text:designed_by: uix.digital	ua	Designed by: uix.digital
text:designed_by:uix.digital	ru	Designed by: uix.digital
text:designed_by:uix.digital	en	Designed by: uix.digital
text:designed_by:uix.digital	ua	Designed by: uix.digital
\.


--
-- Data for Name: platform; Type: TABLE DATA; Schema: public; Owner: shop_admin
--

COPY public.platform (id, group_id, name) FROM stdin;
5	5	XBOX ONE
15	2	PC
16	5	PS3
17	5	XBOX 360
18	5	Nintendo Switch
9	3	Android
2	5	PS4
10	3	IOS
\.


--
-- Data for Name: platform_group; Type: TABLE DATA; Schema: public; Owner: shop_admin
--

COPY public.platform_group (id, name) FROM stdin;
5	Console
3	Mobile
2	PC
\.


--
-- Data for Name: product; Type: TABLE DATA; Schema: public; Owner: shop_admin
--

COPY public.product (id, shop_id, name, url, description, thumbnail_url, currency_id, price_per_unit, stock, min_unit_per_order, game_id, service_id, platform_id, insurance_id, duration_id, online_hrs, offline_hrs, author_id, created_at) FROM stdin;
1	1	тест	test	описание	products/thumbnails/default.jpeg	1	1025	40	10	29	25	15	39	55	11	25	1	2019-12-25 17:42:55.778805
2	1	Моё увожение	moio_uvozhenie	укцукыфв	public/4975382b4e2dda2ff6ff0f04fd0bc35a.1577531918.jpeg	1	1200	40	3	30	22	9	30	48	6	7	1	2019-12-28 11:18:40.626286
\.


--
-- Data for Name: product_delivery_option; Type: TABLE DATA; Schema: public; Owner: shop_admin
--

COPY public.product_delivery_option (id, product_id, delivery_option_id) FROM stdin;
1	1	10
2	2	13
\.


--
-- Data for Name: product_image; Type: TABLE DATA; Schema: public; Owner: shop_admin
--

COPY public.product_image (id, product_id, url) FROM stdin;
7	2	public/4975382b4e2dda2ff6ff0f04fd0bc35a.1577531918.jpeg
\.


--
-- Data for Name: product_option_value; Type: TABLE DATA; Schema: public; Owner: shop_admin
--

COPY public.product_option_value (id, product_id, game_option_value_id) FROM stdin;
1	1	57
2	1	63
3	1	70
4	2	93
\.


--
-- Data for Name: rel_delivery_option_game_service; Type: TABLE DATA; Schema: public; Owner: shop_admin
--

COPY public.rel_delivery_option_game_service (delivery_option_id, game_id, service_id) FROM stdin;
13	28	22
12	28	25
13	28	23
8	29	23
10	29	25
10	29	26
13	30	25
13	30	26
13	30	22
13	31	26
13	31	22
13	32	25
13	32	26
13	32	22
13	34	25
13	34	26
13	34	22
7	36	23
13	36	25
7	36	24
13	36	26
13	38	25
13	38	26
13	38	22
13	39	25
13	39	26
12	39	22
13	40	25
13	40	26
13	40	22
13	41	26
13	42	25
13	42	26
13	42	22
13	43	25
13	43	26
13	43	22
13	44	25
13	44	26
13	44	22
13	45	25
13	45	26
13	45	22
13	46	25
13	46	26
13	46	22
13	47	25
13	47	26
13	47	22
13	48	25
13	48	26
13	48	22
13	49	25
13	49	26
13	49	22
13	50	25
13	50	26
13	50	22
11	51	23
11	51	24
13	51	26
13	51	25
13	52	25
13	52	26
13	52	22
13	53	25
13	53	26
13	53	22
13	54	25
13	54	26
13	54	22
13	55	25
13	55	26
13	55	22
13	56	25
13	56	26
13	56	22
13	58	25
13	58	26
13	58	22
13	59	25
13	59	26
13	59	22
13	60	25
13	60	26
13	60	22
13	61	25
13	61	26
13	61	22
13	62	25
13	62	26
13	62	22
13	63	25
13	63	26
13	63	22
13	65	25
13	65	26
13	65	22
13	66	25
13	66	26
13	66	22
13	68	25
13	68	26
13	68	22
13	69	25
13	69	26
13	69	22
13	70	25
13	70	26
13	70	22
13	71	25
13	71	26
13	71	22
13	72	25
13	72	26
13	72	22
13	73	25
13	73	26
13	73	22
13	74	25
13	74	26
13	74	22
13	75	25
13	75	26
13	75	22
13	76	25
13	76	26
13	76	22
13	77	25
13	77	26
13	77	22
13	78	25
13	78	26
13	78	22
13	79	25
13	79	26
13	79	22
13	80	25
13	80	26
13	80	22
13	81	25
13	81	26
13	81	22
13	82	25
13	82	26
13	82	22
13	83	25
13	83	26
13	83	22
13	84	25
13	84	26
13	84	22
13	85	25
13	85	26
13	85	22
13	86	25
13	86	26
13	86	22
13	87	25
13	87	26
13	87	22
13	88	25
13	88	26
13	88	22
13	89	25
13	89	26
13	89	22
13	90	25
13	90	26
13	90	22
13	91	25
13	91	26
13	91	22
13	92	25
13	92	26
13	92	22
13	93	25
13	93	26
13	93	22
13	94	25
13	94	26
13	94	22
13	95	25
13	95	26
13	95	22
13	96	25
13	96	26
13	96	22
7	97	24
13	97	26
14	97	26
13	97	25
7	98	23
8	98	23
7	98	24
8	98	24
12	98	25
13	98	26
14	98	26
7	99	23
12	99	25
8	100	23
12	100	25
13	102	25
13	102	26
13	102	22
13	103	25
13	103	26
13	103	22
13	104	25
13	104	26
13	104	22
13	105	25
13	105	26
13	105	22
13	106	25
13	106	26
13	106	22
13	107	25
13	107	26
13	107	22
13	108	25
13	108	26
13	108	22
13	109	25
13	109	26
13	109	22
13	110	25
13	110	26
13	110	22
13	111	25
13	111	26
13	111	22
13	112	25
13	112	26
13	112	22
13	113	25
13	113	26
13	113	22
13	114	25
13	114	26
13	114	22
13	115	25
13	115	26
13	115	22
13	116	25
13	116	26
13	116	22
13	117	25
13	117	26
13	117	22
13	118	25
13	118	26
13	118	22
13	119	25
13	119	26
13	119	22
13	99	26
13	41	25
15	120	27
15	120	34
15	120	33
15	120	31
15	120	32
15	120	35
15	120	36
15	120	37
15	120	38
13	120	25
13	120	26
15	120	29
\.


--
-- Data for Name: rel_game_platform; Type: TABLE DATA; Schema: public; Owner: shop_admin
--

COPY public.rel_game_platform (game_id, platform_id) FROM stdin;
28	2
29	15
30	9
30	10
31	9
31	10
32	9
32	10
34	9
34	10
36	15
38	9
38	10
39	9
39	10
40	9
40	10
41	5
41	15
41	2
42	9
42	10
43	9
43	10
44	9
44	10
45	9
45	10
46	9
46	10
47	9
47	10
48	9
48	10
49	9
49	10
50	9
50	10
51	15
52	9
52	10
53	9
53	10
54	9
54	10
55	9
55	10
56	9
58	9
58	10
59	9
59	10
60	9
60	10
61	9
61	10
62	9
62	10
63	9
63	10
65	9
66	9
66	10
68	9
68	10
69	9
70	9
70	10
71	9
71	10
72	9
72	10
73	9
73	10
74	9
74	10
75	9
75	10
76	9
76	10
77	9
77	10
78	9
78	10
79	9
80	9
80	10
81	9
81	10
82	9
82	10
83	9
83	10
84	9
84	10
85	9
85	10
86	9
86	10
87	9
87	10
88	9
88	10
89	9
89	10
90	9
90	10
91	9
91	10
92	9
92	10
93	9
93	10
94	9
94	10
95	9
95	10
96	9
97	5
97	15
97	2
98	5
98	15
98	17
98	2
99	15
100	15
102	9
102	10
103	9
104	9
104	10
105	9
105	10
106	9
106	10
107	9
108	9
108	10
109	9
110	9
110	10
111	9
111	10
112	9
112	10
113	9
113	10
114	9
114	10
115	9
115	10
116	9
116	10
117	9
117	10
118	9
118	10
119	9
119	10
120	15
\.


--
-- Data for Name: rel_service_game; Type: TABLE DATA; Schema: public; Owner: shop_admin
--

COPY public.rel_service_game (game_id, service_id, price_unit_placeholder, advanced_form_name) FROM stdin;
28	22	bait	default.jinja2
28	25	Silver	default.jinja2
28	23	Gold	default.jinja2
29	23	Кинары	default.jinja2
29	25		default.jinja2
29	26		default.jinja2
30	25		default.jinja2
30	26		default.jinja2
30	22	Cash	default.jinja2
31	25		default.jinja2
31	26		default.jinja2
31	22	Golds	default.jinja2
32	25		default.jinja2
32	26		default.jinja2
32	22		default.jinja2
34	25		default.jinja2
34	26		default.jinja2
34	22	Voucher	default.jinja2
36	23	Серебро	default.jinja2
36	25		default.jinja2
36	24		default.jinja2
36	26		default.jinja2
38	25		default.jinja2
38	26		default.jinja2
38	22	Golds	default.jinja2
39	25		default.jinja2
39	26		default.jinja2
39	22	Credits	default.jinja2
40	25		default.jinja2
40	26		default.jinja2
40	22	Tokens	default.jinja2
42	25		default.jinja2
42	26		default.jinja2
42	22	Helix	default.jinja2
43	25		default.jinja2
43	26		default.jinja2
43	22	Coins	default.jinja2
44	25		default.jinja2
44	26		default.jinja2
44	22	Gems	default.jinja2
45	25		default.jinja2
45	26		default.jinja2
45	22	Avacoins	default.jinja2
46	25		default.jinja2
46	26		default.jinja2
46	22	Diamonds	default.jinja2
47	25		default.jinja2
47	26		default.jinja2
47	22	Gems	default.jinja2
48	25		default.jinja2
48	26		default.jinja2
48	22	Pearls	default.jinja2
49	25		default.jinja2
49	26		default.jinja2
49	22	Gold Coins	default.jinja2
50	25		default.jinja2
50	26		default.jinja2
50	22	Diamonds	default.jinja2
51	23	Золото	default.jinja2
51	24		default.jinja2
51	26		default.jinja2
51	25		default.jinja2
52	25		default.jinja2
52	26		default.jinja2
52	22	Gems	default.jinja2
53	25		default.jinja2
53	26		default.jinja2
53	22	Orbs	default.jinja2
54	25		default.jinja2
54	26		default.jinja2
54	22	Crystals	default.jinja2
55	25		default.jinja2
55	26		default.jinja2
55	22	Diamonds	default.jinja2
56	25		default.jinja2
56	26		default.jinja2
56	22	Gems	default.jinja2
58	25		default.jinja2
58	26		default.jinja2
58	22	Orbs	default.jinja2
59	25		default.jinja2
59	26		default.jinja2
59	22	Gems	default.jinja2
60	25		default.jinja2
60	26		default.jinja2
60	22	Gems	default.jinja2
61	25		default.jinja2
61	26		default.jinja2
61	22	Dreamballs	default.jinja2
62	25		default.jinja2
62	26		default.jinja2
62	22	Rubies	default.jinja2
63	25		default.jinja2
63	26		default.jinja2
63	22	Gems	default.jinja2
65	25		default.jinja2
65	26		default.jinja2
65	22	Gems	default.jinja2
66	25		default.jinja2
66	26		default.jinja2
66	22	Diamonds	default.jinja2
68	25		default.jinja2
68	26		default.jinja2
68	22	Gems	default.jinja2
69	25		default.jinja2
69	26		default.jinja2
69	22	Golds	default.jinja2
70	25		default.jinja2
70	26		default.jinja2
70	22	Gems	default.jinja2
71	25		default.jinja2
71	26		default.jinja2
71	22	Diamonds	default.jinja2
72	25		default.jinja2
72	26		default.jinja2
72	22	Diamonds	default.jinja2
73	25		default.jinja2
73	26		default.jinja2
73	22	Credits	default.jinja2
74	25		default.jinja2
74	26		default.jinja2
74	22	Gems	default.jinja2
75	25		default.jinja2
75	26		default.jinja2
75	22	Crystals	default.jinja2
76	25		default.jinja2
76	26		default.jinja2
76	22	Golds	default.jinja2
77	25		default.jinja2
77	26		default.jinja2
77	22	Gems	default.jinja2
78	25		default.jinja2
78	26		default.jinja2
78	22	Gems	default.jinja2
79	25		default.jinja2
79	26		default.jinja2
79	22	Diamonds	default.jinja2
80	25		default.jinja2
80	26		default.jinja2
80	22	Gems	default.jinja2
81	25		default.jinja2
81	26		default.jinja2
81	22	Diamonds	default.jinja2
82	25		default.jinja2
82	26		default.jinja2
82	22	Diamonds	default.jinja2
83	25		default.jinja2
83	26		default.jinja2
83	22	Diamonds	default.jinja2
84	25		default.jinja2
84	26		default.jinja2
84	22	Diamonds	default.jinja2
85	25		default.jinja2
85	26		default.jinja2
85	22	Gems	default.jinja2
86	25		default.jinja2
86	26		default.jinja2
86	22	Goultines	default.jinja2
87	25		default.jinja2
87	26		default.jinja2
87	22	Crowns	default.jinja2
88	25		default.jinja2
88	26		default.jinja2
88	22	Diamantium	default.jinja2
89	25		default.jinja2
89	26		default.jinja2
89	22	Chrono Crystals	default.jinja2
90	25		default.jinja2
90	26		default.jinja2
90	22	Gems	default.jinja2
91	25		default.jinja2
91	26		default.jinja2
91	22	Diamonds	default.jinja2
92	25		default.jinja2
92	26		default.jinja2
92	22	Rubies	default.jinja2
93	25		default.jinja2
93	26		default.jinja2
93	22	Gems	default.jinja2
94	25		default.jinja2
94	26		default.jinja2
94	22	Gems	default.jinja2
95	25		default.jinja2
95	26		default.jinja2
95	22	Gems	default.jinja2
96	25		default.jinja2
96	26		default.jinja2
96	22	Ingots	default.jinja2
97	24		default.jinja2
97	26		default.jinja2
97	25		default.jinja2
98	23	Серебро	default.jinja2
98	24		default.jinja2
98	25		default.jinja2
98	26		default.jinja2
100	23	Золото	default.jinja2
100	25		default.jinja2
102	25		default.jinja2
102	26		default.jinja2
102	22	Ingots	default.jinja2
103	25		default.jinja2
103	26		default.jinja2
103	22	Gems	default.jinja2
104	25		default.jinja2
104	26		default.jinja2
104	22	Skystones	default.jinja2
105	25		default.jinja2
105	26		default.jinja2
105	22	Diamonds	default.jinja2
106	25		default.jinja2
106	26		default.jinja2
106	22	Soul Gems	default.jinja2
107	25		default.jinja2
107	26		default.jinja2
107	22	Balens	default.jinja2
108	25		default.jinja2
108	26		default.jinja2
108	22	Crystals	default.jinja2
109	25		default.jinja2
109	26		default.jinja2
109	22	Gems	default.jinja2
110	25		default.jinja2
110	26		default.jinja2
110	22	Saint Quartez	default.jinja2
111	25		default.jinja2
111	26		default.jinja2
111	22	Fifa Points	default.jinja2
112	25		default.jinja2
112	26		default.jinja2
112	22	Diamonds	default.jinja2
113	25		default.jinja2
113	26		default.jinja2
113	22	Diamonds	default.jinja2
114	25		default.jinja2
114	26		default.jinja2
114	22	Golds	default.jinja2
115	25		default.jinja2
115	26		default.jinja2
115	22	Lapis	default.jinja2
116	25		default.jinja2
116	26		default.jinja2
116	22	Gems	default.jinja2
117	25		default.jinja2
117	26		default.jinja2
117	22	Orbs	default.jinja2
118	25		default.jinja2
118	26		default.jinja2
118	22	Gems	default.jinja2
119	25		default.jinja2
119	26		default.jinja2
119	22	Cash	default.jinja2
99	23	Золото	default.jinja2
99	25		default.jinja2
99	26		default.jinja2
41	26		default.jinja2
41	25		default.jinja2
120	31		default.jinja2
120	33		default.jinja2
120	32		default.jinja2
120	35		default.jinja2
120	36		default.jinja2
120	34		default.jinja2
120	37		default.jinja2
120	38		default.jinja2
120	27		default.jinja2
120	25		default.jinja2
120	26		default.jinja2
120	29		default.jinja2
\.


--
-- Data for Name: service; Type: TABLE DATA; Schema: public; Owner: shop_admin
--

COPY public.service (id, parent_id, name, delivery_guidelines) FROM stdin;
23	0	Currency	Аукцион
22	0	Top Up	Путём захода в ваш игровой аккаунт
24	0	Items	Аукцион
25	0	Accounts	Прямая связь
26	0	Boosting	Путём захода в ваш игровой аккаунт
27	0	Skins	Подарком
29	0	Ключи	трейдом
34	27	Винтовка	cs:go
33	27	Пистолет-пулемет	cs:go
31	27	Пистолет	cs:go
32	27	Дробовик	cs:go
35	27	Снайперская винтовка	cs:go
36	27	Пулемёт	cs:go
37	27	Нож	cs:go
38	27	Перчатки	cs:go
\.


--
-- Data for Name: service_duration; Type: TABLE DATA; Schema: public; Owner: shop_admin
--

COPY public.service_duration (id, service_id, duration_days) FROM stdin;
44	23	3
45	23	6
46	23	9
47	22	7
48	22	30
49	22	14
50	24	7
51	24	14
52	24	30
53	25	7
54	25	14
55	25	30
56	26	7
57	26	14
58	26	30
59	27	7
60	27	30
61	27	14
62	29	7
63	29	14
64	29	30
65	31	7
66	31	14
67	31	30
68	32	7
69	32	14
70	32	30
71	33	7
72	33	14
73	33	30
74	34	7
75	34	14
76	34	30
77	35	7
78	35	14
79	35	30
80	36	7
81	36	14
82	36	30
83	37	7
84	37	14
85	37	30
86	38	7
87	38	14
88	38	30
\.


--
-- Data for Name: service_insurance; Type: TABLE DATA; Schema: public; Owner: shop_admin
--

COPY public.service_insurance (id, service_id, insurance_days) FROM stdin;
29	22	1
30	22	5
31	22	10
32	23	1
33	23	5
34	23	10
35	24	1
36	24	5
37	24	10
38	25	1
39	25	5
40	25	10
41	26	1
42	26	5
43	26	10
47	27	1
48	27	10
49	27	5
50	29	1
51	29	5
52	29	10
53	31	1
54	31	5
55	31	10
56	32	1
57	32	5
58	32	10
59	33	1
60	33	5
61	33	10
62	34	1
63	34	5
64	34	10
65	35	1
66	35	5
67	35	10
68	36	1
69	36	5
70	36	10
71	37	1
72	37	5
73	37	10
74	38	1
75	38	5
76	38	10
\.


--
-- Data for Name: shop; Type: TABLE DATA; Schema: public; Owner: shop_admin
--

COPY public.shop (id, owner_id, name, url, rating, image) FROM stdin;
1	1	GGWH	ggwh	0.0	store_images/default.jpeg
\.


--
-- Data for Name: shop_member; Type: TABLE DATA; Schema: public; Owner: shop_admin
--

COPY public.shop_member (shop_id, user_id, role, role_description) FROM stdin;
\.


--
-- Data for Name: shop_member_permission; Type: TABLE DATA; Schema: public; Owner: shop_admin
--

COPY public.shop_member_permission (shop_id, user_id, permission_id) FROM stdin;
\.


--
-- Name: currency_id_seq; Type: SEQUENCE SET; Schema: public; Owner: shop_admin
--

SELECT pg_catalog.setval('public.currency_id_seq', 1, true);


--
-- Name: delivery_option_id_seq; Type: SEQUENCE SET; Schema: public; Owner: shop_admin
--

SELECT pg_catalog.setval('public.delivery_option_id_seq', 15, true);


--
-- Name: game_id_seq; Type: SEQUENCE SET; Schema: public; Owner: shop_admin
--

SELECT pg_catalog.setval('public.game_id_seq', 120, true);


--
-- Name: game_option_id_seq; Type: SEQUENCE SET; Schema: public; Owner: shop_admin
--

SELECT pg_catalog.setval('public.game_option_id_seq', 169, true);


--
-- Name: game_option_value_id_seq; Type: SEQUENCE SET; Schema: public; Owner: shop_admin
--

SELECT pg_catalog.setval('public.game_option_value_id_seq', 1000, true);


--
-- Name: platform_group_id_seq; Type: SEQUENCE SET; Schema: public; Owner: shop_admin
--

SELECT pg_catalog.setval('public.platform_group_id_seq', 6, true);


--
-- Name: platform_id_seq; Type: SEQUENCE SET; Schema: public; Owner: shop_admin
--

SELECT pg_catalog.setval('public.platform_id_seq', 19, true);


--
-- Name: product_delivery_option_id_seq; Type: SEQUENCE SET; Schema: public; Owner: shop_admin
--

SELECT pg_catalog.setval('public.product_delivery_option_id_seq', 2, true);


--
-- Name: product_id_seq; Type: SEQUENCE SET; Schema: public; Owner: shop_admin
--

SELECT pg_catalog.setval('public.product_id_seq', 2, true);


--
-- Name: product_image_id_seq; Type: SEQUENCE SET; Schema: public; Owner: shop_admin
--

SELECT pg_catalog.setval('public.product_image_id_seq', 7, true);


--
-- Name: product_option_value_id_seq; Type: SEQUENCE SET; Schema: public; Owner: shop_admin
--

SELECT pg_catalog.setval('public.product_option_value_id_seq', 4, true);


--
-- Name: service_duration_id_seq; Type: SEQUENCE SET; Schema: public; Owner: shop_admin
--

SELECT pg_catalog.setval('public.service_duration_id_seq', 88, true);


--
-- Name: service_id_seq; Type: SEQUENCE SET; Schema: public; Owner: shop_admin
--

SELECT pg_catalog.setval('public.service_id_seq', 38, true);


--
-- Name: service_insurance_id_seq; Type: SEQUENCE SET; Schema: public; Owner: shop_admin
--

SELECT pg_catalog.setval('public.service_insurance_id_seq', 76, true);


--
-- Name: shop_id_seq; Type: SEQUENCE SET; Schema: public; Owner: shop_admin
--

SELECT pg_catalog.setval('public.shop_id_seq', 1, true);


--
-- Name: alembic_version alembic_version_pkc; Type: CONSTRAINT; Schema: public; Owner: shop_admin
--

ALTER TABLE ONLY public.alembic_version
    ADD CONSTRAINT alembic_version_pkc PRIMARY KEY (version_num);


--
-- Name: currency currency_name_key; Type: CONSTRAINT; Schema: public; Owner: shop_admin
--

ALTER TABLE ONLY public.currency
    ADD CONSTRAINT currency_name_key UNIQUE (name);


--
-- Name: currency currency_pkey; Type: CONSTRAINT; Schema: public; Owner: shop_admin
--

ALTER TABLE ONLY public.currency
    ADD CONSTRAINT currency_pkey PRIMARY KEY (id);


--
-- Name: delivery_option delivery_option_pkey; Type: CONSTRAINT; Schema: public; Owner: shop_admin
--

ALTER TABLE ONLY public.delivery_option
    ADD CONSTRAINT delivery_option_pkey PRIMARY KEY (id);


--
-- Name: game game_name_key; Type: CONSTRAINT; Schema: public; Owner: shop_admin
--

ALTER TABLE ONLY public.game
    ADD CONSTRAINT game_name_key UNIQUE (name);


--
-- Name: game_option game_option_pkey; Type: CONSTRAINT; Schema: public; Owner: shop_admin
--

ALTER TABLE ONLY public.game_option
    ADD CONSTRAINT game_option_pkey PRIMARY KEY (id);


--
-- Name: game_option_value game_option_value_pkey; Type: CONSTRAINT; Schema: public; Owner: shop_admin
--

ALTER TABLE ONLY public.game_option_value
    ADD CONSTRAINT game_option_value_pkey PRIMARY KEY (id);


--
-- Name: game game_pkey; Type: CONSTRAINT; Schema: public; Owner: shop_admin
--

ALTER TABLE ONLY public.game
    ADD CONSTRAINT game_pkey PRIMARY KEY (id);


--
-- Name: game game_url_key; Type: CONSTRAINT; Schema: public; Owner: shop_admin
--

ALTER TABLE ONLY public.game
    ADD CONSTRAINT game_url_key UNIQUE (url);


--
-- Name: language language_pkey; Type: CONSTRAINT; Schema: public; Owner: shop_admin
--

ALTER TABLE ONLY public.language
    ADD CONSTRAINT language_pkey PRIMARY KEY (lang_code);


--
-- Name: localization localization_pkey; Type: CONSTRAINT; Schema: public; Owner: shop_admin
--

ALTER TABLE ONLY public.localization
    ADD CONSTRAINT localization_pkey PRIMARY KEY (text_code, lang_code);


--
-- Name: platform_group platform_group_pkey; Type: CONSTRAINT; Schema: public; Owner: shop_admin
--

ALTER TABLE ONLY public.platform_group
    ADD CONSTRAINT platform_group_pkey PRIMARY KEY (id);


--
-- Name: platform platform_pkey; Type: CONSTRAINT; Schema: public; Owner: shop_admin
--

ALTER TABLE ONLY public.platform
    ADD CONSTRAINT platform_pkey PRIMARY KEY (id);


--
-- Name: product_delivery_option product_delivery_option_pkey; Type: CONSTRAINT; Schema: public; Owner: shop_admin
--

ALTER TABLE ONLY public.product_delivery_option
    ADD CONSTRAINT product_delivery_option_pkey PRIMARY KEY (id);


--
-- Name: product_image product_image_pkey; Type: CONSTRAINT; Schema: public; Owner: shop_admin
--

ALTER TABLE ONLY public.product_image
    ADD CONSTRAINT product_image_pkey PRIMARY KEY (id);


--
-- Name: product_image product_image_url_key; Type: CONSTRAINT; Schema: public; Owner: shop_admin
--

ALTER TABLE ONLY public.product_image
    ADD CONSTRAINT product_image_url_key UNIQUE (url);


--
-- Name: product_option_value product_option_value_pkey; Type: CONSTRAINT; Schema: public; Owner: shop_admin
--

ALTER TABLE ONLY public.product_option_value
    ADD CONSTRAINT product_option_value_pkey PRIMARY KEY (id);


--
-- Name: product product_pkey; Type: CONSTRAINT; Schema: public; Owner: shop_admin
--

ALTER TABLE ONLY public.product
    ADD CONSTRAINT product_pkey PRIMARY KEY (id);


--
-- Name: rel_delivery_option_game_service rel_delivery_option_game_service_pkey; Type: CONSTRAINT; Schema: public; Owner: shop_admin
--

ALTER TABLE ONLY public.rel_delivery_option_game_service
    ADD CONSTRAINT rel_delivery_option_game_service_pkey PRIMARY KEY (delivery_option_id, game_id, service_id);


--
-- Name: rel_game_platform rel_game_platform_pkey; Type: CONSTRAINT; Schema: public; Owner: shop_admin
--

ALTER TABLE ONLY public.rel_game_platform
    ADD CONSTRAINT rel_game_platform_pkey PRIMARY KEY (game_id, platform_id);


--
-- Name: rel_service_game rel_service_game_pkey; Type: CONSTRAINT; Schema: public; Owner: shop_admin
--

ALTER TABLE ONLY public.rel_service_game
    ADD CONSTRAINT rel_service_game_pkey PRIMARY KEY (game_id, service_id);


--
-- Name: service_duration service_duration_pkey; Type: CONSTRAINT; Schema: public; Owner: shop_admin
--

ALTER TABLE ONLY public.service_duration
    ADD CONSTRAINT service_duration_pkey PRIMARY KEY (id);


--
-- Name: service_insurance service_insurance_pkey; Type: CONSTRAINT; Schema: public; Owner: shop_admin
--

ALTER TABLE ONLY public.service_insurance
    ADD CONSTRAINT service_insurance_pkey PRIMARY KEY (id);


--
-- Name: service service_pkey; Type: CONSTRAINT; Schema: public; Owner: shop_admin
--

ALTER TABLE ONLY public.service
    ADD CONSTRAINT service_pkey PRIMARY KEY (id);


--
-- Name: shop_member_permission shop_member_permission_pkey; Type: CONSTRAINT; Schema: public; Owner: shop_admin
--

ALTER TABLE ONLY public.shop_member_permission
    ADD CONSTRAINT shop_member_permission_pkey PRIMARY KEY (shop_id, user_id, permission_id);


--
-- Name: shop_member shop_member_pkey; Type: CONSTRAINT; Schema: public; Owner: shop_admin
--

ALTER TABLE ONLY public.shop_member
    ADD CONSTRAINT shop_member_pkey PRIMARY KEY (shop_id, user_id);


--
-- Name: shop shop_pkey; Type: CONSTRAINT; Schema: public; Owner: shop_admin
--

ALTER TABLE ONLY public.shop
    ADD CONSTRAINT shop_pkey PRIMARY KEY (id);


--
-- Name: shop shop_url_key; Type: CONSTRAINT; Schema: public; Owner: shop_admin
--

ALTER TABLE ONLY public.shop
    ADD CONSTRAINT shop_url_key UNIQUE (url);


--
-- Name: ix_product_url; Type: INDEX; Schema: public; Owner: shop_admin
--

CREATE INDEX ix_product_url ON public.product USING btree (url);


--
-- Name: ix_shop_owner_id; Type: INDEX; Schema: public; Owner: shop_admin
--

CREATE INDEX ix_shop_owner_id ON public.shop USING btree (owner_id);


--
-- Name: shop_user_idx; Type: INDEX; Schema: public; Owner: shop_admin
--

CREATE INDEX shop_user_idx ON public.shop_member_permission USING btree (shop_id, user_id);


--
-- Name: game_option game_option_game_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: shop_admin
--

ALTER TABLE ONLY public.game_option
    ADD CONSTRAINT game_option_game_id_fkey FOREIGN KEY (game_id) REFERENCES public.game(id) ON DELETE CASCADE;


--
-- Name: game_option game_option_service_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: shop_admin
--

ALTER TABLE ONLY public.game_option
    ADD CONSTRAINT game_option_service_id_fkey FOREIGN KEY (service_id) REFERENCES public.service(id) ON DELETE CASCADE;


--
-- Name: game_option_value game_option_value_game_option_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: shop_admin
--

ALTER TABLE ONLY public.game_option_value
    ADD CONSTRAINT game_option_value_game_option_id_fkey FOREIGN KEY (game_option_id) REFERENCES public.game_option(id) ON DELETE CASCADE;


--
-- Name: localization localization_lang_code_fkey; Type: FK CONSTRAINT; Schema: public; Owner: shop_admin
--

ALTER TABLE ONLY public.localization
    ADD CONSTRAINT localization_lang_code_fkey FOREIGN KEY (lang_code) REFERENCES public.language(lang_code) ON DELETE CASCADE;


--
-- Name: platform platform_group_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: shop_admin
--

ALTER TABLE ONLY public.platform
    ADD CONSTRAINT platform_group_id_fkey FOREIGN KEY (group_id) REFERENCES public.platform_group(id) ON DELETE CASCADE;


--
-- Name: product product_currency_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: shop_admin
--

ALTER TABLE ONLY public.product
    ADD CONSTRAINT product_currency_id_fkey FOREIGN KEY (currency_id) REFERENCES public.currency(id) ON DELETE CASCADE;


--
-- Name: product_delivery_option product_delivery_option_delivery_option_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: shop_admin
--

ALTER TABLE ONLY public.product_delivery_option
    ADD CONSTRAINT product_delivery_option_delivery_option_id_fkey FOREIGN KEY (delivery_option_id) REFERENCES public.delivery_option(id) ON DELETE CASCADE;


--
-- Name: product_delivery_option product_delivery_option_product_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: shop_admin
--

ALTER TABLE ONLY public.product_delivery_option
    ADD CONSTRAINT product_delivery_option_product_id_fkey FOREIGN KEY (product_id) REFERENCES public.product(id) ON DELETE CASCADE;


--
-- Name: product product_duration_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: shop_admin
--

ALTER TABLE ONLY public.product
    ADD CONSTRAINT product_duration_id_fkey FOREIGN KEY (duration_id) REFERENCES public.service_duration(id) ON DELETE CASCADE;


--
-- Name: product product_game_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: shop_admin
--

ALTER TABLE ONLY public.product
    ADD CONSTRAINT product_game_id_fkey FOREIGN KEY (game_id) REFERENCES public.game(id) ON DELETE CASCADE;


--
-- Name: product_image product_image_product_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: shop_admin
--

ALTER TABLE ONLY public.product_image
    ADD CONSTRAINT product_image_product_id_fkey FOREIGN KEY (product_id) REFERENCES public.product(id) ON DELETE CASCADE;


--
-- Name: product product_insurance_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: shop_admin
--

ALTER TABLE ONLY public.product
    ADD CONSTRAINT product_insurance_id_fkey FOREIGN KEY (insurance_id) REFERENCES public.service_insurance(id) ON DELETE CASCADE;


--
-- Name: product_option_value product_option_value_game_option_value_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: shop_admin
--

ALTER TABLE ONLY public.product_option_value
    ADD CONSTRAINT product_option_value_game_option_value_id_fkey FOREIGN KEY (game_option_value_id) REFERENCES public.game_option_value(id) ON DELETE CASCADE;


--
-- Name: product_option_value product_option_value_product_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: shop_admin
--

ALTER TABLE ONLY public.product_option_value
    ADD CONSTRAINT product_option_value_product_id_fkey FOREIGN KEY (product_id) REFERENCES public.product(id) ON DELETE CASCADE;


--
-- Name: product product_platform_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: shop_admin
--

ALTER TABLE ONLY public.product
    ADD CONSTRAINT product_platform_id_fkey FOREIGN KEY (platform_id) REFERENCES public.platform(id) ON DELETE CASCADE;


--
-- Name: product product_service_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: shop_admin
--

ALTER TABLE ONLY public.product
    ADD CONSTRAINT product_service_id_fkey FOREIGN KEY (service_id) REFERENCES public.service(id) ON DELETE CASCADE;


--
-- Name: product product_shop_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: shop_admin
--

ALTER TABLE ONLY public.product
    ADD CONSTRAINT product_shop_id_fkey FOREIGN KEY (shop_id) REFERENCES public.shop(id) ON DELETE CASCADE;


--
-- Name: rel_delivery_option_game_service rel_delivery_option_game_service_delivery_option_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: shop_admin
--

ALTER TABLE ONLY public.rel_delivery_option_game_service
    ADD CONSTRAINT rel_delivery_option_game_service_delivery_option_id_fkey FOREIGN KEY (delivery_option_id) REFERENCES public.delivery_option(id) ON DELETE CASCADE;


--
-- Name: rel_delivery_option_game_service rel_delivery_option_game_service_game_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: shop_admin
--

ALTER TABLE ONLY public.rel_delivery_option_game_service
    ADD CONSTRAINT rel_delivery_option_game_service_game_id_fkey FOREIGN KEY (game_id) REFERENCES public.game(id) ON DELETE CASCADE;


--
-- Name: rel_delivery_option_game_service rel_delivery_option_game_service_service_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: shop_admin
--

ALTER TABLE ONLY public.rel_delivery_option_game_service
    ADD CONSTRAINT rel_delivery_option_game_service_service_id_fkey FOREIGN KEY (service_id) REFERENCES public.service(id) ON DELETE CASCADE;


--
-- Name: rel_game_platform rel_game_platform_game_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: shop_admin
--

ALTER TABLE ONLY public.rel_game_platform
    ADD CONSTRAINT rel_game_platform_game_id_fkey FOREIGN KEY (game_id) REFERENCES public.game(id) ON DELETE CASCADE;


--
-- Name: rel_game_platform rel_game_platform_platform_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: shop_admin
--

ALTER TABLE ONLY public.rel_game_platform
    ADD CONSTRAINT rel_game_platform_platform_id_fkey FOREIGN KEY (platform_id) REFERENCES public.platform(id) ON DELETE CASCADE;


--
-- Name: rel_service_game rel_service_game_game_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: shop_admin
--

ALTER TABLE ONLY public.rel_service_game
    ADD CONSTRAINT rel_service_game_game_id_fkey FOREIGN KEY (game_id) REFERENCES public.game(id) ON DELETE CASCADE;


--
-- Name: rel_service_game rel_service_game_service_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: shop_admin
--

ALTER TABLE ONLY public.rel_service_game
    ADD CONSTRAINT rel_service_game_service_id_fkey FOREIGN KEY (service_id) REFERENCES public.service(id) ON DELETE CASCADE;


--
-- Name: service_duration service_duration_service_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: shop_admin
--

ALTER TABLE ONLY public.service_duration
    ADD CONSTRAINT service_duration_service_id_fkey FOREIGN KEY (service_id) REFERENCES public.service(id) ON DELETE CASCADE;


--
-- Name: service_insurance service_insurance_service_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: shop_admin
--

ALTER TABLE ONLY public.service_insurance
    ADD CONSTRAINT service_insurance_service_id_fkey FOREIGN KEY (service_id) REFERENCES public.service(id) ON DELETE CASCADE;


--
-- Name: shop_member_permission shop_member_permission_shop_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: shop_admin
--

ALTER TABLE ONLY public.shop_member_permission
    ADD CONSTRAINT shop_member_permission_shop_id_fkey FOREIGN KEY (shop_id, user_id) REFERENCES public.shop_member(shop_id, user_id) ON DELETE CASCADE;


--
-- Name: shop_member shop_member_shop_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: shop_admin
--

ALTER TABLE ONLY public.shop_member
    ADD CONSTRAINT shop_member_shop_id_fkey FOREIGN KEY (shop_id) REFERENCES public.shop(id) ON DELETE CASCADE;


--
-- PostgreSQL database dump complete
--

